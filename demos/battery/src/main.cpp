#include <Arduino.h>
#include <U8g2lib.h>
#include <utility>
#include <vector>
#include <string>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

/*
  U8glib Example Overview:
    Frame Buffer Examples: clearBuffer/sendBuffer. Fast, but may not work with all Arduino boards because of RAM consumption
    Page Buffer Examples: firstPage/nextPage. Less RAM usage, should work with all Arduino boards.
    U8x8 Text Only Example: No RAM usage, direct communication with display controller. No graphics, 8x8 Text only.
*/

// Convertion to precentage
// Reference : https://www.helifreak.com/showthread.php?t=333661
const std::vector<std::pair<int, int>> VOLTAGE_TO_PRECENTAGE_CONVERSION = {
    { 2800, 0 },
    { 3123, 15 },
    { 3185, 35 },
    { 3248, 48 },
    { 3380, 83 },
    { 3600, 100 }
};

int analog_voltage_to_battery_precentage(const int voltage)
{
    if (voltage < VOLTAGE_TO_PRECENTAGE_CONVERSION[0].first) {
        return VOLTAGE_TO_PRECENTAGE_CONVERSION[0].second;
    }
    if (voltage > VOLTAGE_TO_PRECENTAGE_CONVERSION.back().first) {
        return VOLTAGE_TO_PRECENTAGE_CONVERSION.back().second;
    }
    std::vector<std::pair<int, int>>::size_type i = 0;
    for (; i != VOLTAGE_TO_PRECENTAGE_CONVERSION.size(); i++) {
        if (voltage == VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first) {
            return VOLTAGE_TO_PRECENTAGE_CONVERSION[i].second;
        }
        if (voltage < VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first) {
            break;
        }
    }
    //Assume linear progress
    int voltage_range = (VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].first);
    int precentage_range = (VOLTAGE_TO_PRECENTAGE_CONVERSION[i].second - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].second);
    return (((voltage - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].first) * precentage_range) / voltage_range) + VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].second;
}

const int analogPin = 36; // potentiometer wiper (middle terminal) connected to analog pin 3
    // outside leads to ground and +5V
int val = 0; // variable to store the value read
int p = 0; //variable to store the p
char str[10]; //variable to store the p as str
char str2[10]; //variable to store the p as str

U8G2_SSD1306_64X48_ER_F_4W_SW_SPI u8g2(U8G2_R0, 21, 22, -1, 4, 27);

void setup()
{
    Serial.begin(9600);
    u8g2.begin();
    adcAttachPin(analogPin);
    analogSetAttenuation(ADC_0db);
}

void loop()
{
    val = analogRead(analogPin);
    p = analog_voltage_to_battery_precentage(val);
    sprintf(str, "%d", p);
    sprintf(str2, "%d", val);
    Serial.println(str);
    u8g2.clearBuffer(); // clear the internal memory
    u8g2.setFont(u8g2_font_ncenB08_tr); // choose a suitable font
    u8g2.drawStr(5, 20, str); // write something to the internal memory
    u8g2.drawStr(5, 40, str2); // write something to the internal memory
    u8g2.sendBuffer(); // transfer internal memory to the display
    delay(2000);
    //@todo: pointless right now because whenever we are connected via usb, the battery shows 0
}
