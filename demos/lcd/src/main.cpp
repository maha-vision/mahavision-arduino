/*

  HelloWorld.ino

  Universal 8bit Graphics Library (https://github.com/olikraus/u8g2/)

  Copyright (c) 2016, olikraus@gmail.com
  All rights reserved.

  Redistribution and use in source and binary forms, with or without modification, 
  are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this list 
    of conditions and the following disclaimer.
    
  * Redistributions in binary form must reproduce the above copyright notice, this 
    list of conditions and the following disclaimer in the documentation and/or other 
    materials provided with the distribution.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND 
  CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR 
  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
  STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF 
  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  

*/

#include <Arduino.h>
#include <U8g2lib.h>
#include <algorithm>
#include <string>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

/*
  U8glib Example Overview:
    Frame Buffer Examples: clearBuffer/sendBuffer. Fast, but may not work with all Arduino boards because of RAM consumption
    Page Buffer Examples: firstPage/nextPage. Less RAM usage, should work with all Arduino boards.
    U8x8 Text Only Example: No RAM usage, direct communication with display controller. No graphics, 8x8 Text only.
*/

// Maybe define as 255?
#define U8G2_ESP32_HAL_UNDEFINED (-1)

// CLK - GPIO21 / D0
#define PIN_CLK 21

// MOSI - GPIO 22 / D1
#define PIN_MOSI 22

// RESET - GPIO 27
#define PIN_RESET 27

// DC - GPIO 4
#define PIN_DC 4

//U8G2_SSD1306_128X64_NONAME_F_4W_SW_SPI
U8G2_SSD1306_64X48_ER_F_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/PIN_CLK, /* data=*/PIN_MOSI, /* cs=*/U8G2_ESP32_HAL_UNDEFINED, /* dc=*/PIN_DC, /* reset=*/PIN_RESET);

#define israel_flag_width 40
#define israel_flag_height 29
static const unsigned char israel_flag_bits[] PROGMEM = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18,
   0x00, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x80, 0xff, 0x01, 0x00,
   0x00, 0x80, 0x24, 0x01, 0x00, 0x00, 0x00, 0xc3, 0x00, 0x00, 0x00, 0x00,
   0xc3, 0x00, 0x00, 0x00, 0x00, 0xc3, 0x00, 0x00, 0x00, 0x80, 0x24, 0x01,
   0x00, 0x00, 0x80, 0xff, 0x01, 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00,
   0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00 };

#define easter_egg_2_2_width 35
#define easter_egg_2_2_height 48
static const unsigned char easter_egg_2_2_bits[] PROGMEM = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x18, 0x00, 0x00, 0x00, 0xf8,
   0x6a, 0x00, 0x00, 0x00, 0x94, 0x51, 0x01, 0x00, 0x00, 0x52, 0x49, 0x01,
   0x00, 0x00, 0x5d, 0xa4, 0x05, 0x00, 0x00, 0xcf, 0xbf, 0x07, 0x00, 0x80,
   0xcf, 0x7b, 0x0f, 0x00, 0xc0, 0xcf, 0x7b, 0x1f, 0x00, 0x20, 0xfe, 0xfb,
   0x07, 0x00, 0xe0, 0xf9, 0x7f, 0x3d, 0x00, 0xe0, 0xa1, 0x4f, 0x3c, 0x00,
   0xe0, 0xff, 0xff, 0xbf, 0x00, 0xf0, 0xf9, 0xff, 0x7e, 0x00, 0xf0, 0xf1,
   0x3f, 0x7e, 0x00, 0xf8, 0xf5, 0xbf, 0xfe, 0x00, 0xfc, 0xf3, 0xff, 0xfe,
   0x01, 0xdc, 0xff, 0x78, 0xdf, 0x01, 0x9c, 0xff, 0x78, 0xdf, 0x01, 0xdc,
   0xbf, 0xe8, 0xdf, 0x00, 0xd0, 0x3f, 0xe5, 0x73, 0x00, 0xd0, 0x3e, 0xe5,
   0x63, 0x00, 0x24, 0x7e, 0xf5, 0x03, 0x01, 0xfc, 0xfb, 0x7f, 0xbd, 0x02,
   0xe8, 0xc9, 0x1c, 0xf9, 0x01, 0xd2, 0xf9, 0x7a, 0x8d, 0x06, 0x82, 0x7f,
   0xf5, 0x87, 0x06, 0x0b, 0x7f, 0xf5, 0xa7, 0x06, 0x0f, 0x3f, 0xe5, 0x37,
   0x07, 0xe7, 0xfb, 0xf8, 0x3c, 0x07, 0xe7, 0xf3, 0xf8, 0x3c, 0x07, 0xff,
   0xf3, 0xfc, 0xfd, 0x07, 0xfc, 0xf1, 0xff, 0xfc, 0x01, 0xfc, 0xfb, 0x7f,
   0xfd, 0x01, 0xfc, 0xff, 0xff, 0xff, 0x01, 0xfe, 0xff, 0xff, 0xff, 0x03,
   0x1c, 0xf9, 0x5f, 0xf0, 0x01, 0x04, 0xfd, 0xff, 0xa2, 0x01, 0xe0, 0xff,
   0xfc, 0xbf, 0x00, 0xf8, 0xf9, 0x7c, 0xff, 0x00, 0xf0, 0xf3, 0x3c, 0xdf,
   0x00, 0x90, 0x37, 0xb2, 0xc7, 0x00, 0x00, 0x07, 0xe3, 0x43, 0x00, 0x40,
   0xdc, 0xae, 0x3d, 0x00, 0x80, 0xad, 0x92, 0x1d, 0x00, 0x00, 0xd8, 0x5a,
   0x02, 0x00, 0x00, 0xf0, 0x70, 0x00, 0x00, 0x00, 0x00, 0x1f, 0x00, 0x00 };

std::string hebrew_str = "מחנטט";
int i = 0;

void setup(void)
{
    Serial.begin(9600);
    Serial.println("setup lcd demo");
    std::reverse(hebrew_str.begin(), hebrew_str.end());
    u8g2.begin();
   //u8g2.setContrast(255);
}

void draw_english()
{
    u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
    u8g2.drawUTF8(5, 20, "hello"); // write something to the internal memory
    u8g2.drawStr(5, 40, "world"); // write something to the internal memory
}

void draw_hebrew()
{
    u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
    u8g2.drawUTF8(20, 20, hebrew_str.c_str()); // write something to the internal memory
    u8g2.drawStr(5, 40, "2018"); // write something to the internal memory
}

void draw_circle()
{
    u8g2.drawCircle(20, 20, 5);
}

void draw_glyph()
{
    u8g2.setFont(u8g2_font_unifont_t_symbols); // choose a suitable font
    u8g2.drawGlyph(20, 20, 0x2603);
}

void draw_israel_flag()
{
  u8g2.drawXBMP(0, 0, israel_flag_width, israel_flag_height, israel_flag_bits);
}

void draw_easter_egg()
{
  u8g2.drawXBMP(0, 0, easter_egg_2_2_width, easter_egg_2_2_height, easter_egg_2_2_bits);
}

void showcase()
{
  u8g2.clearBuffer(); // clear the internal memory
  switch (i % 6) {
  case 0:
      draw_english();
      break;
  case 1:
      draw_hebrew();
      break;
  case 2:
      draw_circle();
      break;
  case 3:
      draw_glyph();
      break;
  case 4:
      draw_israel_flag();
      break;
  case 5:
      draw_easter_egg();
      break;        
  }
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}

void test_hebrew()
{
  u8g2.clearBuffer(); // clear the internal memory
  switch (i % 4) {
  case 0:
      u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "0 טנחמ"); // write something to the internal memory
      break;
  case 1:
      u8g2.setFont(u8g2_font_6x13_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "1 טנחמ"); // write something to the internal memory
      break;
  case 2:
      u8g2.setFont(u8g2_font_6x13B_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "2 טנחמ"); // write something to the internal memory
      break;
  case 3:
  	  u8g2.setFont(u8g2_font_cu12_t_hebrew); // choose a suitable font
  	  u8g2.drawUTF8(20, 20, "3 טנחמ"); // write something to the internal memory
      break;     
  }
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}


void test_english()
{
  u8g2.clearBuffer(); // clear the internal memory
  switch (i % 4) {
  case 0:
      u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "mahanet 0"); // write something to the internal memory
      break;
  case 1:
      u8g2.setFont(u8g2_font_6x13_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "mahanet 1"); // write something to the internal memory
      break;
  case 2:
      u8g2.setFont(u8g2_font_6x13B_t_hebrew); // choose a suitable font
      u8g2.drawUTF8(20, 20, "mahanet 2"); // write something to the internal memory
      break;
  case 3:
  	  u8g2.setFont(u8g2_font_cu12_t_hebrew); // choose a suitable font
  	  u8g2.drawUTF8(20, 20, "mahanet 3"); // write something to the internal memory
      break;     
  }
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}

void test_color_mode_and_contrast()
{
	// setDrawColor 0 or 1
	// setFontMode 0 or 1
	// setContrast 0 to 255

  u8g2.clearBuffer(); // clear the internal memory
  u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
  switch (i % 8) {
  case 0:
      u8g2.setDrawColor(0);
      u8g2.setFontMode(0);
      u8g2.setContrast(10);
      u8g2.drawUTF8(20, 20, "0 טנחמ"); // write something to the internal memory
      break;
  case 1:
      u8g2.setDrawColor(0);
      u8g2.setFontMode(0);
      u8g2.setContrast(240);
      u8g2.drawUTF8(20, 20, "1 טנחמ"); // write something to the internal memory
      break;
  case 2:
  	  u8g2.setDrawColor(0);
  	  u8g2.setFontMode(1);
  	  u8g2.setContrast(10);
      u8g2.drawUTF8(20, 20, "2 טנחמ"); // write something to the internal memory
      break;
  case 3:
  	  u8g2.setDrawColor(0);
  	  u8g2.setFontMode(1);
  	  u8g2.setContrast(240);
  	  u8g2.drawUTF8(20, 20, "3 טנחמ"); // write something to the internal memory
      break;     
  case 4:
      u8g2.setDrawColor(1);
      u8g2.setFontMode(0);
      u8g2.setContrast(10);
      u8g2.drawUTF8(20, 20, "4 טנחמ"); // write something to the internal memory
      break;
  case 5:
      u8g2.setDrawColor(1);
      u8g2.setFontMode(0);
      u8g2.setContrast(240);
      u8g2.drawUTF8(20, 20, "5 טנחמ"); // write something to the internal memory
      break;
  case 6:
  	  u8g2.setDrawColor(1);
  	  u8g2.setFontMode(1);
  	  u8g2.setContrast(10);
      u8g2.drawUTF8(20, 20, "6 טנחמ"); // write something to the internal memory
      break;
  case 7:
  	  u8g2.setDrawColor(1);
  	  u8g2.setFontMode(1);
  	  u8g2.setContrast(240);
  	  u8g2.drawUTF8(20, 20, "7 בלה טנחמ"); // write something to the internal memory
      break;   
  }
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}

void test_font_mode()
{
 u8g2.clearBuffer(); // clear the internal memory
  u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
  switch (i % 2) {
  case 0:
      u8g2.setDrawColor(1);
      u8g2.setFontMode(0);
      u8g2.setContrast(240);
      u8g2.drawUTF8(20, 20, "טנחמ"); // write something to the internal memory
      Serial.println("0");
      break;
  case 1:
  	  u8g2.setDrawColor(1);
  	  u8g2.setFontMode(1);
  	  u8g2.setContrast(240);
  	  u8g2.drawUTF8(20, 20, "טנחמ"); // write something to the internal memory
  	  Serial.println("1");
      break;   
  }
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}

void genericHebrew()
{
  u8g2.clearBuffer(); // clear the internal memory
  u8g2.setFont(u8g2_font_unifont_t_hebrew); // choose a suitable font
  std::string str;
  switch (i % 5) {
  case 0:
      str = "טנחמ םולש";
      break;
  case 1:
      str = "פאאחמ";
      break;
  case 2:
      str = "םיכורב";
      break;
  case 3:
      str = "םיאבה";
      break;
  case 4:
      str = "הרוק המ";
      break;   
  }
  u8g2.drawUTF8(0, 20, str.c_str());
  u8g2.sendBuffer(); // transfer internal memory to the display
  i++;
  delay(2000);
}

void loop(void)
{
    showcase();
}  
