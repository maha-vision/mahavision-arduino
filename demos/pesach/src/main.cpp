#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

U8G2_SSD1306_64X48_ER_F_4W_SW_SPI u8g2(U8G2_R2, 21, 22, -1, 4, 27);
const int button1Pin = 32;
const int button2Pin = 33;
const int button3Pin = 25;

void setup(void)
{
    u8g2.begin();
    pinMode(button1Pin, INPUT);
    pinMode(button2Pin, INPUT);
    pinMode(button3Pin, INPUT);
}

int wait_count = 0;

void loop(void)
{
    u8g2.clearBuffer(); // clear the internal memory
    u8g2.setFont(u8g2_font_6x13_t_hebrew); // choose a suitable font

    int count = 0;

    if (HIGH == digitalRead(button1Pin)) {
        u8g2.drawStr(5, 15, "Pesach");
        count++;
    }
    if (HIGH == digitalRead(button2Pin)) {
        u8g2.drawStr(5, 30, "Maza");
        count++;
    }
    if (HIGH == digitalRead(button3Pin)) {
        u8g2.drawStr(5, 45, "Maror");
        count++;
    }

    if (count == 3) {
      if (wait_count > 5)
      {
        u8g2.clearBuffer();
        u8g2.drawStr(5, 15, "Chag");
        u8g2.drawStr(5, 30, "Pesach");
        u8g2.drawStr(5, 45, "Sameach!");
        delay(3000);
      }
      wait_count++;
    } else {
      wait_count = 0;
    }

    u8g2.sendBuffer(); // transfer internal memory to the display
    delay(100);
}
