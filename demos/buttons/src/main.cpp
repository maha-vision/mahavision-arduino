#include "button_event_handler.hpp"
#include "event_based_button.hpp"
#include <Arduino.h>
#include <array>
#include <cmath>

//Mv stands for MahaVision

enum Direction {
    Forward,
    Backward
};

class MvButtonsEventHandler {
public:
    virtual void onPressed()
    {
        Serial.printf("onPressed\n");
    }

    virtual void onHeld(uint16_t duration, uint16_t repeatCount)
    {
        Serial.printf("onHeld %d %d\n", duration, repeatCount);
    }

    virtual void onSwiped(Direction direction)
    {
        Serial.printf("onSwiped %s\n", direction == Forward ? "forward" : "backward");
    }
};

class MvButtons {
public:
    class MvButtonEventHandler : public ButtonEventHandler {
    public:
        MvButtonEventHandler(int button_index, MvButtons& buttons)
            : button_index(button_index)
            , buttons(buttons)
        {
        }

        void onButtonPressed()
        {
            buttons.buttons[button_index].last_pressed_time = millis();
            buttons.onButtonPressed();
        }

        void onButtonHeld(uint16_t duration, uint16_t repeatCount)
        {
            buttons.buttons[button_index].last_held_time = millis();
            buttons.buttons[button_index].last_held_duration = duration;
            buttons.buttons[button_index].last_held_repeat_count = repeatCount;
            buttons.onButtonHeld();
        }

        void onButtonReleased(uint16_t duration)
        {
        }

    private:
        int button_index;
        MvButtons& buttons;
    };

    struct MvButton {
        struct MvButtonConfiguration {
            MvButtonConfiguration(uint8_t pin, int button_index)
                : pin(pin)
                , button_index(button_index)
            {
            }
            uint8_t pin;
            int button_index;
        };

        MvButton(MvButtonConfiguration config, MvButtons& buttons)
            : config(config)
            , buttons(buttons)
            , handler(config.button_index, buttons)
            , button(config.pin, handler)
        {
        }

        MvButtonConfiguration config;
        MvButtons& buttons;
        MvButtonEventHandler handler;
        EventBasedButton button;
        unsigned long last_pressed_time = -1;
        unsigned long last_held_time = -1;
        unsigned long last_held_duration = -1;
        unsigned long last_held_repeat_count = -1;
    };

    struct MvButtonsConfiguration {
        std::array<MvButton::MvButtonConfiguration, 3> buttonConfigurations = { { MvButton::MvButtonConfiguration(25, 0), MvButton::MvButtonConfiguration(33, 1), MvButton::MvButtonConfiguration(32, 2) } };
        uint16_t onHoldDuration = 1000;
    };

    MvButtons(MvButtonsConfiguration config, MvButtonsEventHandler& handler)
        : config(config)
        , handler(handler)
        , buttons({ { MvButton(config.buttonConfigurations[0], *this), MvButton(config.buttonConfigurations[1], *this), MvButton(config.buttonConfigurations[2], *this) } })
    {
    }

    void onButtonPressed()
    {
        Serial.printf("pressed: btn0 %d btn1 %d btn2 %d\n", buttons[0].last_pressed_time, buttons[1].last_pressed_time, buttons[2].last_pressed_time);

        unsigned long diff1 = std::abs((signed long)buttons[0].last_pressed_time - (signed long)buttons[1].last_pressed_time);
        unsigned long diff2 = std::abs((signed long)buttons[1].last_pressed_time - (signed long)buttons[2].last_pressed_time);
        unsigned long diff3 = std::abs((signed long)buttons[0].last_pressed_time - (signed long)buttons[2].last_pressed_time);

        Serial.printf("diffs: diff1 %d diff2 %d diff3 %d\n", diff1, diff2, diff3);

        int min_swipe_diff = 50;
        int max_swipe_diff = 300;
        if (diff1 < max_swipe_diff && diff1 > min_swipe_diff && diff2 < max_swipe_diff && diff2 > min_swipe_diff && diff3 < max_swipe_diff * 2 && diff3 > min_swipe_diff * 2) {
            Direction direction = (signed long)buttons[0].last_pressed_time - (signed long)buttons[1].last_pressed_time >= 0 ? Forward : Backward;
            handler.onSwiped(direction);
        } else {
            handler.onPressed();
        }
    }

    void onButtonHeld()
    {
        Serial.printf("held: btn0 (%d, %d, %d) btn1 (%d, %d, %d) btn2 (%d, %d, %d)\n",
            buttons[0].last_held_time, buttons[0].last_held_duration, buttons[0].last_held_repeat_count,
            buttons[1].last_held_time, buttons[1].last_held_duration, buttons[1].last_held_repeat_count,
            buttons[2].last_held_time, buttons[2].last_held_duration, buttons[2].last_held_repeat_count);

        unsigned long diff1 = std::abs((signed long)buttons[0].last_held_time - (signed long)buttons[1].last_held_time);
        unsigned long diff2 = std::abs((signed long)buttons[1].last_held_time - (signed long)buttons[2].last_held_time);
        unsigned long diff3 = std::abs((signed long)buttons[0].last_held_time - (signed long)buttons[2].last_held_time);

        Serial.printf("diffs: diff1 %d diff2 %d diff3 %d\n", diff1, diff2, diff3);

        int max_held_diff = 100;
        if (diff1 < max_held_diff && diff2 < max_held_diff && diff3 < max_held_diff) {
            handler.onHeld(buttons[0].last_held_duration, buttons[0].last_held_repeat_count);
        }
    }

    void begin()
    {
        for (auto& button : buttons) {
            button.button.begin();
        }
    }

    void update()
    {
        for (auto& button : buttons) {
            button.button.update();
        }
    }

    MvButtonsConfiguration config;
    MvButtonsEventHandler& handler;
    std::array<MvButton, 3> buttons;
};

MvButtonsEventHandler handler;
MvButtons buttons(MvButtons::MvButtonsConfiguration(), handler);

void setup()
{
    // Open up the serial port so that we can write to it
    Serial.begin(9600);

    buttons.begin();
}

void loop()
{
    // Check the state of the button
    buttons.update();
    delay(1);
}

//@todo: one convention.
