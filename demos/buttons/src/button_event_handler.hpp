#pragma once

#include <BasicButton.h>
#include <Button.h>
#include <ButtonEventCallback.h>

class ButtonEventHandler {
public:
    virtual void onButtonPressed();

    // duration reports back how long it has been since the button was originally pressed.
    // repeatCount tells us how many times this function has been called by this button.
    virtual void onButtonHeld(uint16_t duration, uint16_t repeatCount);

    // duration reports back the total time that the button was held down
    virtual void onButtonReleased(uint16_t duration);
};
