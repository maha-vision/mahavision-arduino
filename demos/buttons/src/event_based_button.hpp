#pragma once

#include "button_event_handler.hpp"
#include <BasicButton.h>
#include <Button.h>
#include <ButtonEventCallback.h>

//@todo: should be inner class of EventBasedButton
struct EventBasedButtonConfiguration
{
    EventBasedButtonConfiguration(uint8_t pin);
    uint8_t pin;
    uint16_t onReleaseWait = 0;
    uint16_t onReleaseMaxWait = -1;
    uint16_t onHoldDuration = 1000;
    uint16_t onHoldRepeatEvery = 500;
};

class EventBasedButton : public BasicButton {
public:
    EventBasedButton(EventBasedButtonConfiguration config, ButtonEventHandler& buttonEventHandler);

    void begin();

    ButtonEventHandler& buttonEventHandler();

private:
    EventBasedButtonConfiguration config;
    ButtonEventHandler& handler;
};
