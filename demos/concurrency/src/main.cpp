/*Simple example of multi loop
* By Evandro Luis Copercini
* Based on pcbreflux video
* Public domain license 2017
*/

#include <Arduino.h>

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

TaskHandle_t loop2_xHandle;

void loop1(void *pvParameters) {
  while (1) {
     Serial.print("loop1 with param ");
     Serial.println((long)pvParameters);
     delay(1000);
  }
}

void loop2(void *pvParameters) {
  while (1) {
     Serial.print("loop2 with param ");
     Serial.println((long)pvParameters);
     delay(300);
  }
}

void loop3(void *pvParameters) {
  while (1) {
     Serial.print("loop3 with param ");
     Serial.println((long)pvParameters);
     delay(4000);
  }
}

int count = 0;

void loop4(void *pvParameters) {
  while (1) {
     Serial.print("loop4 with param ");
     Serial.println((long)pvParameters);
     delay(2000);
     count++;
     if (count == 5) {
         Serial.println("deleting task loop2");
         vTaskDelete(loop2_xHandle);
     }
  }
}

void setup() {
  Serial.begin(9600);
  xTaskCreatePinnedToCore(loop1, "loop1", 4096, (void*) 13, 1, NULL, ARDUINO_RUNNING_CORE);
  xTaskCreatePinnedToCore(loop2, "loop2", 4096, (void*) 77, 1, &loop2_xHandle, ARDUINO_RUNNING_CORE);
  xTaskCreatePinnedToCore(loop3, "loop3", 4096, (void*) 42, 1, NULL, ARDUINO_RUNNING_CORE);
  xTaskCreate(loop4, "loop4", 4096, (void*) 100, 1, NULL);
}

void loop() {
   Serial.println("loop0");
   delay(5000);
}

/*
deploy:
pio run -t upload --upload-port /dev/ttyUSB0

show serial:
pio device monitor -p /dev/ttyUSB0 --dtr 0 --rts 0
*/
