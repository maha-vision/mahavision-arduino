/*
 * IRremote: IRrecvDemo - demonstrates receiving IR codes with IRrecv
 * An IR detector/demodulator must be connected to the input RECV_PIN.
 * Version 0.1 July, 2009
 * Copyright 2009 Ken Shirriff
 * http://arcfn.com
 */

#include <IRremote.h>
#include <map>

int RECV_PIN = 16;

IRrecv irrecv(RECV_PIN);

decode_results results;

int messageLen = 0;
const int MAX_POSSIBLE_MESSAGE = 10000;
std::map<int, std::string> messageParts;

void setup()
{
  Serial.begin(9600);
  irrecv.enableIRIn(); // Start the receiver
}

void gotBeginPacket(unsigned long packetData)
{
    byte bytes[3] = { 0 };
    packetData = ((packetData<<4)>>8);
    bytes[0] = byte ((packetData>>16));
    bytes[1] = byte ((packetData<<8>>16));
    bytes[2] = byte ((packetData<<16>>16));
    int len = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
    Serial.printf("got begin packet with len: %d\n", len);
    messageLen = len;
    messageParts.clear();
}

void gotDataPacket(unsigned long packetData)
{
    char message[4] = { 0 };
    int index = packetData >> 28;
    packetData = ((packetData<<4)>>8);
    message[0] = char ((packetData>>16) + '0');
    message[1] = char ((packetData<<8>>16) + '0');
    message[2] = char ((packetData<<16>>16) + '0');
    Serial.printf("got data packet with message %s and index %d\n", message, index);
    messageParts[index] = message;


    Serial.printf("messageParts.size() is %d, messageLen is %d\n", (messageParts.size()), messageLen);
    if (messageParts.size() == messageLen)
    {
        std::string message = "";
        for (int i = 0 ; i < messageLen ; i++)
        {
            message += messageParts[i];
        }
        Serial.printf("full message is %s\n", message.c_str());
    }
}

void gotPacket(unsigned long packetData)
{
    int val = packetData & 0x0000000f;
    if (val == 11)
    {
        gotBeginPacket(packetData);
    }
    else if (val == 10)
    {
        gotDataPacket(packetData);
    }
}

void dump(decode_results *results) {
    // Dumps out the decode_results structure.
    // Call this after IRrecv::decode()
    
    Serial.printf("in dump, results->decode_type is %d, results->rawlen is %d, results->value is ", results->decode_type, results->rawlen);
    Serial.println(results->value, HEX);
    if (results->decode_type == NEC) 
    {
        gotPacket(results->value);
    }
}

void checkReceive()
{
    if (irrecv.decode(&results)) {
        dump(&results);
        irrecv.resume(); // Receive the next value
    }
}

void loop() {
    checkReceive();
}

/*
unsigned long msg1=0x0;
unsigned long msg2=0x0;
unsigned long msg3=0x0;
unsigned long msg4=0x0;
unsigned long splirIr(unsigned long IrDataIn )
{
 //split into byte array no needed
  //byte IrBuffer[4];
  //IrBuffer[0] = IrDataIn >> 24;
  //IrBuffer[1] = IrDataIn >> 16;
  //IrBuffer[2] = IrDataIn >> 8;
  //IrBuffer[3] = IrDataIn >> 0;
  // end split to array

  // start 4 bit A
  //Serial.println(IrDataIn >> 28,HEX);
  
  //stop  4 bit A
  //Serial.println(IrDataIn & 0x0000000f,HEX);
  
  // Extract data
  //Serial.println((IrDataIn<<4)>>8 ,HEX);
  
  if ((IrDataIn >> 28 > 9) &&(IrDataIn & 0x0000000f) == 0xA)
  {
    switch(IrDataIn >> 28)
  {
    case 0xa:
    msg1 = ((IrDataIn<<4)>>8);
      break;
    case 0xb:
    msg2 = ((IrDataIn<<4)>>8);
      break;  
    case 0xc:
    msg3 = ((IrDataIn<<4)>>8);
      break;
    case 0xd:
    msg4 = ((IrDataIn<<4)>>8);
      break;  
    default:
      break;    
  }    
  ///start bit A recived && stop A
      return ((IrDataIn<<4)>>8);
  }
  else
      return 0x1;
}

if(msg1 != 0x0 && msg2 !=0x0 && msg3!=0x0 && msg4!=0x0)
    {
      Serial.print("full MSG REAL TEXT : ");
      char full[12];
      full[0] = char ((msg1>>16) + '0');
      full[1] = char ((msg1<<8>>16) + '0');
      full[2] = char ((msg1<<16>>16) + '0');
      full[3] = char ((msg2>>16) + '0');
      full[4] = char ((msg2<<8>>16) + '0');
      full[5] = char ((msg2<<16>>16) + '0');
      full[6] = char ((msg3>>16) + '0');
      full[7] = char ((msg3<<8>>16) + '0');
      full[8] = char ((msg3<<16>>16) + '0');
      full[9] = char ((msg4>>16) + '0');
      full[10] = char ((msg4<<8>>16) + '0');
      full[11] = char ((msg4<<16>>16) + '0');      
      Serial.println(full);
      msg1=0x0;msg2=0x0;msg3=0x0;msg4=0x0;
    }
*/

