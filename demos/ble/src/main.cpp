/*
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleServer.cpp
    Ported to Arduino ESP32 by Evandro Copercini
*/

#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <memory>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

//#define SERVICE_UUID "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
//#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

//#define SERVICE_UUID "ABBA"
//#define CHARACTERISTIC_UUID "DADA"


BLEService* createService(BLEServer* pServer, std::string serviceUuid)
{
    BLEService* pService = pServer->createService(serviceUuid);
    return pService;
}

BLECharacteristic* createCharacteristic(BLEService* pService, std::string characteristicUuid, std::string initialValue)
{
    BLECharacteristic* pCharacteristic = pService->createCharacteristic(
            characteristicUuid,
            BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE);

    pCharacteristic->setValue(initialValue);
    return pCharacteristic;
}

void setup()
{
    Serial.begin(9600);
    Serial.println("Starting BLE work!");

    BLEDevice::init("MyESP32");
    BLEServer* pServer = BLEDevice::createServer();
    BLEAdvertising* pAdvertising = pServer->getAdvertising();

    auto pService1 = createService(pServer, "1100");
    auto pService1Char1 = createCharacteristic(pService1, "1111", "1111");

    pService1->start();
    pAdvertising->start();

    Serial.println("you should read 1111 now");
    delay(20000);

    auto pService1Char2 = createCharacteristic(pService1, "1122", "1122");

    auto pService2 = createService(pServer, "2200");
    auto pService2Char1 = createCharacteristic(pService2, "2211", "2211");
    pService2->start();

    Serial.println("you should read also 1122 and 2211 now");
    delay(20000);

    pAdvertising->stop();

    auto pService1Char3 = createCharacteristic(pService1, "1133", "1133");
    auto pService3 = createService(pServer, "3300");
    auto pService3Char1 = createCharacteristic(pService3, "3311", "3311");
    pService3->start();

    pAdvertising->start();

    Serial.println("you should read also 1133 and 3311 now");
    delay(20000);

}

void loop()
{
}
