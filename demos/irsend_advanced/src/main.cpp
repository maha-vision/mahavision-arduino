#include <IRremote.h>
#include <string>
#include <vector>
#include <array>

int SEND_PIN = 17;

const int PACKET_LEN = 3;

const int MAX_POSSIBLE_MESSAGE = 10000;

IRsend irsend(SEND_PIN);

void setup()
{
    Serial.begin(9600);
}

void sendSinglePacket(std::string singleMessage, int index)
{
	// assumes packe len is 3
	Serial.printf("sendSinglePacket %s %d\n", singleMessage.c_str(), index);

	byte bytesToSend[3] = { 0 };
	for(int i = 0 ; i < 3 ; i++)
	{
		bytesToSend[i] = singleMessage[i] - '0';
  	}
  	uint32_t msg = 0;
  	msg |= (uint32_t)0xa << 0| (uint32_t)bytesToSend[2] << 4| (uint32_t)bytesToSend[1] << 12| (uint32_t)bytesToSend[0] << 20| (uint32_t)(index) << 28;
  	Serial.println(msg, HEX);
  	irsend.sendNEC(msg, 32);
}

void sendBeginPacket(int messageSize)
{
	Serial.printf("sendBeginPacket %d\n", messageSize);
	byte bytesToSend[3] = { 0 };
	for(int i = 0 ; i < 3 ; i++)
	{
		bytesToSend[i] = (messageSize >> (8*i)) & 0xff;
  	}

  	uint32_t msg = 0;
  	msg |= (uint32_t)0xb << 0| (uint32_t)bytesToSend[2] << 4| (uint32_t)bytesToSend[1] << 12| (uint32_t)bytesToSend[0] << 20| (uint32_t)0 << 28;
  	Serial.println(msg, HEX);
  	irsend.sendNEC(msg, 32);
}

void sendMessage(std::string message)
{
	if (message.size() > MAX_POSSIBLE_MESSAGE)
	{
		return;
	}

	Serial.printf("sendMessage %s\n", message.c_str());
	while (message.size() % PACKET_LEN != 0)
	{
		message += "$";
	}

    sendBeginPacket(message.size() / 3);

    int index = 0;
    while (index < message.size() / PACKET_LEN)
    {
    	sendSinglePacket(message.substr(index * PACKET_LEN, PACKET_LEN), index);
        delay(50);
        sendSinglePacket(message.substr(index * PACKET_LEN, PACKET_LEN), index);
        delay(50);
        index++;
    }
}

void loop() {
	sendMessage("ABBA CACA 123123");
	delay(1000);
}



/*

void IrSender(char Id[])
{
  byte byteTosSend[12];
  for(int i =0; i<12;i++)
  {
  byteTosSend[i] = Id[i] - '0';
  }

	uint32_t msg1,msg2,msg3,msg4 = 0;
	msg1 |= (uint32_t)0xa << 0| (uint32_t)byteTosSend[2] << 4| (uint32_t)byteTosSend[1] << 12| (uint32_t)byteTosSend[0] << 20| (uint32_t)0xa << 28;
	msg2 |= (uint32_t)0xa << 0| (uint32_t)byteTosSend[5] << 4| (uint32_t)byteTosSend[4] << 12| (uint32_t)byteTosSend[3] << 20| (uint32_t)0xb << 28;
	msg3 |= (uint32_t)0xa << 0| (uint32_t)byteTosSend[8] << 4| (uint32_t)byteTosSend[7] << 12| (uint32_t)byteTosSend[6] << 20| (uint32_t)0xc << 28;
	msg4 |= (uint32_t)0xa << 0| (uint32_t)byteTosSend[11] << 4| (uint32_t)byteTosSend[10] << 12| (uint32_t)byteTosSend[9] << 20| (uint32_t)0xd << 28;

	Serial.println(msg1,HEX);
	Serial.println(msg2,HEX);
	Serial.println(msg3,HEX);
	Serial.println(msg4,HEX);

//reverse

	Serial.println(char('0' + ((msg1<<4)>>24)));
	for(int i=0; i<4;i++); //send repeat
	  {
	    irsend.sendNEC(msg1, 32);
	    delay(30);
	    irsend.sendNEC(msg2, 32);
	    delay(30);
	    irsend.sendNEC(msg3, 32);
	    delay(30);
	    irsend.sendNEC(msg4, 32);
	    delay(30);
	  }
}
*/