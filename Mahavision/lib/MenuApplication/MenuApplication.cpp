#include "MenuApplication.hpp"
#include <ButtonsEvents.hpp>
#include <BleEvents.hpp>
#include <Applications.hpp>
#include <Config.hpp>

class ChangeApplicationOnSwipeEventHandler : public events::EventHandler
{
public:
	ChangeApplicationOnSwipeEventHandler(MenuApplication& menuApp)
		:menuApp(menuApp)
	{

	}

    void handle(events::EventPtr event)
    {
    	logging::debug("in ChangeApplicationOnSwipeEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsSwipedEvent>(event);
		if (menuApp.inMenu) {
			menuApp.updateApplicationIndex(realEvent->direction == buttons::Forward);
		}
    }
private:
	MenuApplication& menuApp;
};

class SelectAppOnFrontButtonHeldEventHandler : public events::EventHandler
{
public:
    SelectAppOnFrontButtonHeldEventHandler(MenuApplication& menuApp)
    : menuApp(menuApp)
    {

    }
    void handle(events::EventPtr event)
    {
        logging::debug("in SelectAppOnFrontButtonHeldEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsFrontHeldEvent>(event);
        if (realEvent->duration > config::minimumHeldTimeForMenu)
        {
            if (menuApp.inMenu)
        	{
        		menuApp.chooseCurrentApplication();
        	}
        }
    }
private:
    MenuApplication& menuApp;
};

class ReturnToMenuOnBackButtonHeldEventHandler : public events::EventHandler
{
public:
    ReturnToMenuOnBackButtonHeldEventHandler(MenuApplication& menuApp)
    : menuApp(menuApp)
    {

    }
    void handle(events::EventPtr event)
    {
        logging::debug("in ReturnToMenuOnBackButtonHeldEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsBackHeldEvent>(event);
        if (realEvent->duration > config::minimumHeldTimeForMenu)
        {
           if (!menuApp.inMenu)
        	{
        		menuApp.backToMenu();
        	}
        }
    }
private:
    MenuApplication& menuApp;
};

MenuApplication::MenuApplication()
	: inMenu(true)
	, applicationIndex(0)
	, lcd(nullptr)
	, eventBus(nullptr)
	, currentApplication(nullptr)
{

}

void MenuApplication::updateApplicationIndex(bool forward)
{
	if (!inMenu)
	{
		return;
	}

	logging::debug("applicationIndex is %d, forward is %d, items.size() is %d\n", applicationIndex, forward, APPlLICATIONS.size());
	applicationIndex += forward ? 1 : -1;
	if (applicationIndex > APPlLICATIONS.size()) // overflow
	{
		applicationIndex = APPlLICATIONS.size() - 1;
	}

	if (applicationIndex == APPlLICATIONS.size())
	{
		applicationIndex = 0;
	}
	drawCurrentApplicationPreview();
}

void MenuApplication::drawCurrentApplicationPreview()
{
	if (!inMenu)
	{
		return;
	}
	logging::debug("applicationIndex is %d\n", applicationIndex);
	std::get<0>(APPlLICATIONS[applicationIndex])(lcd);
}

void MenuApplication::chooseCurrentApplication()
{
	if (!inMenu)
	{
		return;
	}
	currentApplication = std::get<1>(APPlLICATIONS[applicationIndex])();
	logging::debug("currentApplication is %lx\n", currentApplication);
	currentApplication->begin(eventBus, lcd);
	inMenu = false;
}

void MenuApplication::backToMenu()
{
	if (inMenu)
	{
		return;
	}
	currentApplication->finish();
	inMenu = true;
	drawCurrentApplicationPreview();
}

void MenuApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
	lcd = lcd_;
	eventBus = eventBus_;

    lcd->erase();
    lcd->run1HebrewSentencesAnimation("טירפת");
    delay(1000);

    eventBus->subscribe(buttons::BTN_SWIPED_EVENT, std::make_shared<ChangeApplicationOnSwipeEventHandler>(*this));
	eventBus->subscribe(buttons::BTN_FRONT_HELD_EVENT, std::make_shared<SelectAppOnFrontButtonHeldEventHandler>(*this));
    eventBus->subscribe(buttons::BTN_BACK_HELD_EVENT, std::make_shared<ReturnToMenuOnBackButtonHeldEventHandler>(*this));

    drawCurrentApplicationPreview();
}

void MenuApplication::finish()
{
    
}
