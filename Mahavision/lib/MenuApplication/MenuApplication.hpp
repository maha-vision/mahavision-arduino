#pragma once

#include <Application.hpp>

class MenuApplication : public Application
{
public:
	MenuApplication();

    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();

	friend class ChangeApplicationOnSwipeEventHandler;
	friend class SelectAppOnFrontButtonHeldEventHandler;
    friend class ReturnToMenuOnBackButtonHeldEventHandler;

private:
	void updateApplicationIndex(bool forward);
	void drawCurrentApplicationPreview();
	void chooseCurrentApplication();
	void backToMenu();
	bool inMenu;
	size_t applicationIndex;
	display::LcdPtr lcd;
	events::EventBusPtr eventBus;
	ApplicationPtr currentApplication;
};
