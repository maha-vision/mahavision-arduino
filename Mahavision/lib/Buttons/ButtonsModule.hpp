#pragma once

#include <EventBasedButton.hpp>
#include <ButtonEventHandler.hpp>
#include <EventBus.hpp>
#include <array>
#include <cmath>
#include <Module.hpp>

namespace buttons
{
    class ButtonsModule : public Module {
    public:
        struct ButtonConfiguration {
            ButtonConfiguration(uint8_t pin, int buttonIndex);
            uint8_t pin;
            int buttonIndex;
        };

        struct ButtonsModuleConfiguration {
            std::array<ButtonsModule::ButtonConfiguration, 3> buttonConfigurations = { { ButtonsModule::ButtonConfiguration(25, 0), ButtonsModule::ButtonConfiguration(33, 1), ButtonsModule::ButtonConfiguration(32, 2) } };
            uint16_t onHoldDuration = 500;
        };

        ButtonsModule();
        ButtonsModule(ButtonsModuleConfiguration config);

        //@todesign: should be private
        void onButtonPressed();

        //@todesign: should be private
        void onButtonHeld();

        void begin(events::EventBusPtr eventBus);

        void update();
    
    private:
        class TheButtonEventHandler : public ButtonEventHandler {
        public:
            TheButtonEventHandler(int buttonIndex, buttons::ButtonsModule& buttons);
            
            void onButtonPressed();

            void onButtonHeld(uint16_t duration, uint16_t repeatCount);

            void onButtonReleased(uint16_t duration);
        private:
            int buttonIndex;
            buttons::ButtonsModule& buttons;
        };

        struct Button {
            Button(ButtonConfiguration config, buttons::ButtonsModule& buttons);

            ButtonConfiguration config;
            ButtonsModule& buttons;
            TheButtonEventHandler handler;
            EventBasedButton button;
            unsigned long lastPressedTime = -1;
            unsigned long lastHeldTime = -1;
            unsigned long lastHeldDuration = -1;
            unsigned long lastHeldRepeatCount = -1;
        };

        ButtonsModuleConfiguration config;
        events::EventBusPtr eventBus;
        std::array<Button, 3> buttons;
    };
};
