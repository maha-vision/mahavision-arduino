#include "ButtonsModule.hpp"
#include "ButtonsEvents.hpp"
#include <Thread.hpp>
#include <Logging.hpp>

buttons::ButtonsModule::TheButtonEventHandler::TheButtonEventHandler(int buttonIndex, buttons::ButtonsModule& buttons)
    : buttonIndex(buttonIndex)
    , buttons(buttons)
    {

    }

void buttons::ButtonsModule::TheButtonEventHandler::onButtonPressed()
{
    buttons.buttons[buttonIndex].lastPressedTime = millis();
    buttons.onButtonPressed();
}

void buttons::ButtonsModule::TheButtonEventHandler::onButtonHeld(uint16_t duration, uint16_t repeatCount)
{
    buttons.buttons[buttonIndex].lastHeldTime = millis();
    buttons.buttons[buttonIndex].lastHeldDuration = duration;
    buttons.buttons[buttonIndex].lastHeldRepeatCount = repeatCount;
    buttons.onButtonHeld();
}

void buttons::ButtonsModule::TheButtonEventHandler::onButtonReleased(uint16_t duration)
{
}

buttons::ButtonsModule::ButtonConfiguration::ButtonConfiguration(uint8_t pin, int buttonIndex)
    : pin(pin)
    , buttonIndex(buttonIndex)
{
}

buttons::ButtonsModule::Button::Button(ButtonConfiguration config, ButtonsModule& buttons)
            : config(config)
            , buttons(buttons)
            , handler(config.buttonIndex, buttons)
            , button(config.pin, handler)
        {
        }

buttons::ButtonsModule::ButtonsModule()
    : config(ButtonsModuleConfiguration())
    , eventBus(nullptr)
    , buttons({ { Button(config.buttonConfigurations[0], *this), Button(config.buttonConfigurations[1], *this), Button(config.buttonConfigurations[2], *this) } })
{
}

buttons::ButtonsModule::ButtonsModule(ButtonsModuleConfiguration config)
    : config(config)
    , eventBus(nullptr)
    , buttons({ { Button(config.buttonConfigurations[0], *this), Button(config.buttonConfigurations[1], *this), Button(config.buttonConfigurations[2], *this) } })
{
}

void buttons::ButtonsModule::onButtonPressed()
{
    unsigned long currentTime = millis();

    logging::debug("pressed: btn0 %d btn1 %d btn2 %d\n", buttons[0].lastPressedTime, buttons[1].lastPressedTime, buttons[2].lastPressedTime);

    unsigned long diff1 = std::abs((signed long)buttons[0].lastPressedTime - (signed long)buttons[1].lastPressedTime);
    unsigned long diff2 = std::abs((signed long)buttons[1].lastPressedTime - (signed long)buttons[2].lastPressedTime);
    unsigned long diff3 = std::abs((signed long)buttons[0].lastPressedTime - (signed long)buttons[2].lastPressedTime);

    logging::debug("diffs: diff1 %d diff2 %d diff3 %d\n", diff1, diff2, diff3);

    //@todo: better consts
    const int maxPressedUpdateTime = 10;
    const int minSwipeDiff = 10;
    const int maxSwipeDiff = 400;
    if (diff1 < maxSwipeDiff && diff1 > minSwipeDiff && diff2 < maxSwipeDiff && diff2 > minSwipeDiff && diff3 < maxSwipeDiff * 2 && diff3 > minSwipeDiff * 2) {
        Direction direction = (signed long)buttons[0].lastPressedTime - (signed long)buttons[1].lastPressedTime >= 0 ? Backward : Forward;
        logging::debug("buttons swiped %d\n", direction);
        eventBus->publish(std::make_shared<ButtonsSwipedEvent>(direction));
    } else {
        if (currentTime - buttons[0].lastPressedTime < maxPressedUpdateTime)
        {
            eventBus->publish(std::make_shared<ButtonsBackPressedEvent>());
        }
        if (currentTime - buttons[1].lastPressedTime < maxPressedUpdateTime)
        {
            eventBus->publish(std::make_shared<ButtonsMiddlePressedEvent>());
        }
        if (currentTime - buttons[2].lastPressedTime < maxPressedUpdateTime)
        {
            eventBus->publish(std::make_shared<ButtonsFrontPressedEvent>());
        }
    }
}

void buttons::ButtonsModule::onButtonHeld()
{
    unsigned long currentTime = millis();

    unsigned long diff1 = std::abs((signed long)buttons[0].lastHeldTime - (signed long)buttons[1].lastHeldTime);
    unsigned long diff2 = std::abs((signed long)buttons[1].lastHeldTime - (signed long)buttons[2].lastHeldTime);
    unsigned long diff3 = std::abs((signed long)buttons[0].lastHeldTime - (signed long)buttons[2].lastHeldTime);

    logging::debug("time: %d\n", millis());
    logging::debug("button 0: %d %d %d\n", buttons[0].lastHeldTime, buttons[0].lastHeldDuration, buttons[0].lastHeldRepeatCount);
    logging::debug("button 1: %d %d %d\n", buttons[1].lastHeldTime, buttons[1].lastHeldDuration, buttons[1].lastHeldRepeatCount);
    logging::debug("button 2: %d %d %d\n", buttons[2].lastHeldTime, buttons[2].lastHeldDuration, buttons[2].lastHeldRepeatCount);
    logging::debug("--------------\n");

    const int maxHeldUpdateTime = 10;
    if (currentTime - buttons[0].lastHeldTime < maxHeldUpdateTime)
    {
        eventBus->publish(std::make_shared<ButtonsBackHeldEvent>(buttons[0].lastHeldDuration, buttons[0].lastHeldRepeatCount));
    }
    if (currentTime - buttons[1].lastHeldTime < maxHeldUpdateTime)
    {
        eventBus->publish(std::make_shared<ButtonsMiddleHeldEvent>(buttons[1].lastHeldDuration, buttons[1].lastHeldRepeatCount));
    }
    if (currentTime - buttons[2].lastHeldTime < maxHeldUpdateTime)
    {
        eventBus->publish(std::make_shared<ButtonsFrontHeldEvent>(buttons[2].lastHeldDuration, buttons[2].lastHeldRepeatCount));
    }

    const int maxHeldDiff = 100;
    if (diff1 < maxHeldDiff && diff2 < maxHeldDiff && diff3 < maxHeldDiff) {
        logging::debug("buttons held %d %d\n", buttons[0].lastHeldDuration, buttons[0].lastHeldRepeatCount);
        eventBus->publish(std::make_shared<ButtonsHeldEvent>(buttons[0].lastHeldDuration, buttons[0].lastHeldRepeatCount));
    }
}

void buttons::ButtonsModule::begin(events::EventBusPtr eventBus_)
{
    eventBus = eventBus_;
    for (auto& button : buttons) {
        button.button.begin();
    }
}

void buttons::ButtonsModule::update()
{    
    for (auto& button : buttons) {
        button.button.update();
    }
}
