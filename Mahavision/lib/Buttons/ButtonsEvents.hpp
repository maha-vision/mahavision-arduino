#include <Event.hpp>

namespace buttons {

    const events::EventType BTN_PRESSED_EVENT = (events::EventType)1000;
    const events::EventType BTN_SWIPED_EVENT = (events::EventType)1001;
    const events::EventType BTN_HELD_EVENT = (events::EventType)1002;
    const events::EventType BTN_FRONT_HELD_EVENT = (events::EventType)1003;
    const events::EventType BTN_MIDDLE_HELD_EVENT = (events::EventType)1004;
    const events::EventType BTN_BACK_HELD_EVENT = (events::EventType)1005;
    const events::EventType BTN_FRONT_PRESSED_EVENT = (events::EventType)1006;
    const events::EventType BTN_MIDDLE_PRESSED_EVENT = (events::EventType)1007;
    const events::EventType BTN_BACK_PRESSED_EVENT = (events::EventType)1008;

    enum Direction {
        Forward,
        Backward
    };

    struct ButtonsPressedEvent : public events::Event
    {
        events::EventType type()
        {
            return BTN_PRESSED_EVENT;
        }
    };

    struct ButtonsFrontPressedEvent : public events::Event
    {
        events::EventType type()
        {
            return BTN_FRONT_PRESSED_EVENT;
        }
    };

    struct ButtonsMiddlePressedEvent : public events::Event
    {
        events::EventType type()
        {
            return BTN_MIDDLE_PRESSED_EVENT;
        }
    };

    struct ButtonsBackPressedEvent : public events::Event
    {
        events::EventType type()
        {
            return BTN_BACK_PRESSED_EVENT;
        }
    };

    struct ButtonsSwipedEvent : public events::Event
    {
        ButtonsSwipedEvent(Direction direction)
            :direction(direction)
        {

        }
        events::EventType type()
        {
            return BTN_SWIPED_EVENT;
        }
        Direction direction;
    };

    struct ButtonsHeldEvent : public events::Event
    {
        ButtonsHeldEvent(uint16_t duration, uint16_t repeatCount)
            :duration(duration), repeatCount(repeatCount)
        {

        }
        events::EventType type()
        {
            return BTN_HELD_EVENT;
        }
        uint16_t duration;
        uint16_t repeatCount;
    };

    struct ButtonsFrontHeldEvent : public events::Event
    {
        ButtonsFrontHeldEvent(uint16_t duration, uint16_t repeatCount)
            :duration(duration), repeatCount(repeatCount)
        {

        }
        events::EventType type()
        {
            return BTN_FRONT_HELD_EVENT;
        }
        uint16_t duration;
        uint16_t repeatCount;
    };

    struct ButtonsMiddleHeldEvent : public events::Event
    {
        ButtonsMiddleHeldEvent(uint16_t duration, uint16_t repeatCount)
            :duration(duration), repeatCount(repeatCount)
        {

        }
        events::EventType type()
        {
            return BTN_MIDDLE_HELD_EVENT;
        }
        uint16_t duration;
        uint16_t repeatCount;
    };

    struct ButtonsBackHeldEvent : public events::Event
    {
        ButtonsBackHeldEvent(uint16_t duration, uint16_t repeatCount)
            :duration(duration), repeatCount(repeatCount)
        {

        }
        events::EventType type()
        {
            return BTN_BACK_HELD_EVENT;
        }
        uint16_t duration;
        uint16_t repeatCount;
    };

}
