#include "Info.hpp"
#include <Logging.hpp>
#include <Preferences.h>
#include <Config.hpp>

static Preferences preferences;
static std::map<std::string, std::string> values;

void info::initialize()
{
	logging::debug("initializing info.\n");
	set("batteryPercentage", "100");
    
}

std::string info::get_persistent(const std::string& key)
{
    preferences.begin(config::preferencesNamespaceName.c_str(), true);

    //@todo: handle exception
    std::string result = preferences.getString(key.c_str(), config::infoDefaultValues.at(key).c_str()).c_str();

    preferences.end();

    return result;
}

void info::set_persistent(const std::string& key, const std::string& value)
{
    preferences.begin(config::preferencesNamespaceName.c_str(), false);

    preferences.putString(key.c_str(), value.c_str());

    preferences.end();
}

std::string info::get(const std::string& key)
{
    return values[key];
}

void info::set(const std::string& key, const std::string& value)
{
    values[key] = value;
}
