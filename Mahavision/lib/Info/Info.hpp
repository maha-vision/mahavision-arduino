#pragma once

#include <string>

struct info
{
	static void initialize();

	static std::string get(const std::string& key);
	static void set(const std::string& key, const std::string& value);

	static std::string get_persistent(const std::string& key);
	static void set_persistent(const std::string& key, const std::string& value);
};
