#include "Thread.hpp"

multithreading::Thread::Thread(ThreadFunction function, ThreadFunctionArgument argument, int stackSize)
    :alive(false),
    stackSize(stackSize)
{
    xTaskCreate(function, "", stackSize, argument, 2, &handle);
    alive = true;
}

bool multithreading::Thread::isAlive()
{
    return alive;
}

void multithreading::Thread::kill()
{
    if (alive)
    {
        vTaskDelete(handle);
        alive = false;
    }
}
