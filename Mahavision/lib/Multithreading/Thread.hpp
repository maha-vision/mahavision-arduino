#pragma once

#include <vector>
#include <Arduino.h>
#include <memory>

#if CONFIG_FREERTOS_UNICORE
#define ARDUINO_RUNNING_CORE 0
#else
#define ARDUINO_RUNNING_CORE 1
#endif

namespace multithreading
{
    typedef void (*ThreadFunction)(void*);
    typedef void* ThreadFunctionArgument;

    class Thread
    {
    public:
        Thread(ThreadFunction function, ThreadFunctionArgument argument, int stackSize);
        bool isAlive();
        void kill();
    private:
        TaskHandle_t handle;
        bool alive;
        int stackSize;
    };

    typedef std::shared_ptr<Thread> ThreadPtr;
}
