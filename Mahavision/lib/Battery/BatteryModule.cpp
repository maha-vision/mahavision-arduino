#include "BatteryModule.hpp"
#include "BatteryEvents.hpp"
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
#include <cmath>
#include <sstream>
#include <string>

const std::vector<std::pair<int, int>> VOLTAGE_TO_PRECENTAGE_CONVERSION = {
    { 2800, 0 },
    { 3123, 15 },
    { 3185, 35 },
    { 3248, 48 },
    { 3380, 83 },
    { 3600, 100 }
};

static int analog_voltage_to_battery_precentage(const int voltage)
{
    if (voltage < VOLTAGE_TO_PRECENTAGE_CONVERSION[0].first) {
        return VOLTAGE_TO_PRECENTAGE_CONVERSION[0].second;
    }
    if (voltage > VOLTAGE_TO_PRECENTAGE_CONVERSION.back().first) {
        return VOLTAGE_TO_PRECENTAGE_CONVERSION.back().second;
    }
    std::vector<std::pair<int, int>>::size_type i = 0;
    for (; i != VOLTAGE_TO_PRECENTAGE_CONVERSION.size(); i++) {
        if (voltage == VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first) {
            return VOLTAGE_TO_PRECENTAGE_CONVERSION[i].second;
        }
        if (voltage < VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first) {
            break;
        }
    }
    //Assume linear progress
    int voltage_range = (VOLTAGE_TO_PRECENTAGE_CONVERSION[i].first - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].first);
    int precentage_range = (VOLTAGE_TO_PRECENTAGE_CONVERSION[i].second - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].second);
    return (((voltage - VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].first) * precentage_range) / voltage_range) + VOLTAGE_TO_PRECENTAGE_CONVERSION[i - 1].second;
}

battery::BatteryModule::BatteryModule()
    : eventBus(nullptr)
    , pin(batteryPin)
{
}

battery::BatteryModule::BatteryModule(uint8_t pin)
    : eventBus(nullptr)
    , pin(pin)
{
}


void battery::BatteryModule::begin(events::EventBusPtr eventBus_)
{
    eventBus = eventBus_;
    adcAttachPin(pin);
    analogSetAttenuation(ADC_0db);
    randomSeed(analogRead(0));
}

void battery::BatteryModule::update()
{    
    uint16_t batteryADCValue = 0;
    uint16_t batteryPercentage = 0;
    batteryADCValue = analogRead(pin);

    batteryPercentage = analog_voltage_to_battery_precentage(batteryADCValue);

    if(std::abs(previousBatteryPercentage - batteryPercentage) > 5) {
        previousBatteryPercentage = batteryPercentage;
        std::ostringstream s;
        s << batteryPercentage;
        info::set("batteryPercentage", s.str());
        eventBus->publish(std::make_shared<BatteryChangedEvent>(batteryPercentage));
    }
}
