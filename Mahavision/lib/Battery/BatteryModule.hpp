#pragma once

#include <EventBus.hpp>
#include <Module.hpp>

const int batteryPin = 36;

namespace battery
{
    class BatteryModule : public Module {
    public:
        BatteryModule();
        BatteryModule(uint8_t pin);

        void begin(events::EventBusPtr eventBus);

        void update();
    
    private:
        events::EventBusPtr eventBus;
        uint8_t pin;
        uint16_t previousBatteryPercentage;
    };
};
