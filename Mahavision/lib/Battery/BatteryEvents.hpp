#include <Event.hpp>

namespace battery {

    const events::EventType BATTERY_CHANGED_EVENT = (events::EventType)4000;


    struct BatteryChangedEvent : public events::Event
    {
        BatteryChangedEvent(uint8_t batteryChargePrecent)
            :batteryChargePrecent(batteryChargePrecent)
        {

        }
        events::EventType type()
        {
            return BATTERY_CHANGED_EVENT;
        }
        uint8_t batteryChargePrecent;
    };

}
