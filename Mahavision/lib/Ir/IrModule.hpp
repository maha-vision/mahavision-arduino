#include <Arduino.h>
#include <BetterIRremote.h>
#include <Module.hpp>
#include <vector>

namespace ir
{
    class IrModule : public Module {
    public:
        struct IrModuleConfiguration {
            uint8_t receivePin = 16;
            uint8_t sendPin = 17;
        };

        IrModule(IrModuleConfiguration config);
        IrModule();

        void begin(events::EventBusPtr eventBus);

        void update();

        friend class RequestIrSendMessageEventHandler;
        
    private:
        void sendBeginPacket(int packetCount);
        void sendDataPacket(std::string data, int index);
        void sendMessage(std::string message);

        void receiveBeacon(unsigned long data);
        void receiveBeginPacket(unsigned long data);
        void receiveDataPacket(unsigned long data);
        void checkReceive();

        IrModuleConfiguration config;
        events::EventBusPtr eventBus;
        IRrecv irRecv;
        IRsend irSend;
        int messagePartCount;
        std::map<int, std::string> messageParts;

        const int PACKET_LEN = 3;
        const int MAX_POSSIBLE_MESSAGE = 10000;
    };
}
