#include "IrModule.hpp"
#include "IrEvents.hpp"
#include <Logging.hpp>

ir::IrModule::IrModule(IrModuleConfiguration config)
    : config(config),
    eventBus(nullptr),
    irRecv(config.receivePin),
    irSend(config.sendPin),
    messagePartCount(-1),
    messageParts()
{
}

ir::IrModule::IrModule()
    : config(IrModuleConfiguration()),
    eventBus(nullptr),
    irRecv(config.receivePin),
    irSend(config.sendPin),
    messagePartCount(-1),
    messageParts()
{
}

namespace ir
{
    class RequestIrSendMessageEventHandler : public events::EventHandler
    {
    public:
        RequestIrSendMessageEventHandler(ir::IrModule& irModule)
            :irModule(irModule)
        {

        }

        void handle(events::EventPtr event)
        {
            auto realEvent = std::static_pointer_cast<ir::RequestIrMessageSendEvent>(event);
            irModule.sendMessage(realEvent->message);
        }

    private:
        ir::IrModule& irModule;
    };
}

void ir::IrModule::sendBeginPacket(int packetCount)
{
    logging::debug("sendBeginPacket %d\n", packetCount);
    byte bytesToSend[3] = { 0 };
    for(int i = 0 ; i < 3 ; i++)
    {
        bytesToSend[i] = (packetCount >> (8*i)) & 0xff;
    }

    uint32_t msg = 0;
    msg |= (uint32_t)0xb << 0| (uint32_t)bytesToSend[2] << 4| (uint32_t)bytesToSend[1] << 12| (uint32_t)bytesToSend[0] << 20| (uint32_t)0 << 28;
    irSend.sendNEC(msg, 32);
}

void ir::IrModule::sendDataPacket(std::string data, int index)
{
    logging::debug("sendDataPacket %s %d\n", data.c_str(), index);

    byte bytesToSend[3] = { 0 };
    for(int i = 0 ; i < 3 ; i++)
    {
        bytesToSend[i] = data[i] - '0';
    }
    uint32_t msg = 0;
    msg |= (uint32_t)0xa << 0| (uint32_t)bytesToSend[2] << 4| (uint32_t)bytesToSend[1] << 12| (uint32_t)bytesToSend[0] << 20| (uint32_t)(index) << 28;
    irSend.sendNEC(msg, 32);
}

void ir::IrModule::sendMessage(std::string message)
{
    if (message.size() > MAX_POSSIBLE_MESSAGE)
    {
        logging::fatal("message size too big...\n");
        return;
    }

    logging::debug("sendMessage %s\n", message.c_str());
    while (message.size() % PACKET_LEN != 0)
    {
        message += "$";
    }

    sendBeginPacket(message.size() / 3);
    delay(50);
    sendBeginPacket(message.size() / 3);
    delay(50);

    int index = 0;
    while (index < message.size() / PACKET_LEN)
    {
        sendDataPacket(message.substr(index * PACKET_LEN, PACKET_LEN), index);
        delay(50);
        sendDataPacket(message.substr(index * PACKET_LEN, PACKET_LEN), index);
        delay(50);
        index++;
    }
    irSend.disableIROut(); // Turn led off for ESP32
}

void ir::IrModule::receiveBeacon(unsigned long packetData)
{
    byte bytes[4] = { 0 };
    packetData = ((packetData<<4)>>8);
    bytes[0] = byte ((packetData>>16));
    bytes[1] = byte ((packetData<<8>>16));
    bytes[2] = byte ((packetData<<16>>16));
    int data = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
    logging::debug("receiveBeaconPacket with data: %d\n", data);
    eventBus->publish(std::make_shared<IrBeaconReceivedEvent>
                (data));
}

void ir::IrModule::receiveBeginPacket(unsigned long packetData)
{
    byte bytes[4] = { 0 };
    packetData = ((packetData<<4)>>8);
    bytes[0] = byte ((packetData>>16));
    bytes[1] = byte ((packetData<<8>>16));
    bytes[2] = byte ((packetData<<16>>16));
    int count = bytes[0] + (bytes[1] << 8) + (bytes[2] << 16) + (bytes[3] << 24);
    logging::debug("receiveBeginPacket with len: %d\n", count);
    messagePartCount = count;
    messageParts.clear();
}

void ir::IrModule::receiveDataPacket(unsigned long packetData)
{
    char message[4] = { 0 };
    int index = packetData >> 28;
    packetData = ((packetData<<4)>>8);
    message[0] = char ((packetData>>16) + '0');
    message[1] = char ((packetData<<8>>16) + '0');
    message[2] = char ((packetData<<16>>16) + '0');
    logging::debug("receiveDataPacket with message %s and index %d\n", message, index);
    messageParts[index] = message;

    eventBus->publish(std::make_shared<IrPacketReceivedEvent>
                (message));

    if (messageParts.size() == messagePartCount)
    {
        std::string message = "";
        for (int i = 0 ; i < messagePartCount ; i++)
        {
            message += messageParts[i];
        }

        // remove padding ('$'')
        std::string output;
        for (size_t i = 0; i < message.size(); ++i)
        {
          if (message[i] != '$')
            {
                output += message[i];
            }
        }
        message = output;

        logging::debug("full message is %s\n", message.c_str());
        messageParts.clear();

        eventBus->publish(std::make_shared<IrMessageReceivedEvent>
                (message));
    }
}

void ir::IrModule::checkReceive()
{
    decode_results results;
    if (irRecv.decode(&results)) {
        logging::debug("results.decode_type is %d, results.value is %d\n", results.decode_type, results.value);
        if (results.decode_type == NEC)
        {
            int val = results.value & 0x0000000f;
            if (val == 0xc)
            {
                receiveBeacon(results.value);   
            }
            else if (val == 0xb)
            {
                receiveBeginPacket(results.value);
            }
            else if (val == 0xa)
            {
                receiveDataPacket(results.value);
            }
        }
        irRecv.resume(); // Receive the next value
    }
}

void ir::IrModule::begin(events::EventBusPtr eventBus_)
{
    eventBus = eventBus_;
    eventBus->subscribe(ir::REQUEST_IR_MESSAGE_SEND_EVENT, std::make_shared<ir::RequestIrSendMessageEventHandler>(*this));
    irRecv.enableIRIn();
}

void ir::IrModule::update()
{
    checkReceive();
}
