#include <Arduino.h>
#include <Event.hpp>
#include <vector>


namespace ir {

    const events::EventType IR_PACKET_RECEIVED_EVENT = (events::EventType)2000;
    const events::EventType IR_MESSAGE_RECEIVED_EVENT = (events::EventType)2001;
    const events::EventType REQUEST_IR_MESSAGE_SEND_EVENT = (events::EventType)2002;
    const events::EventType IR_BEACON_RECEIVED_EVENT = (events::EventType)2003;

    struct IrPacketReceivedEvent : public events::Event
    {
        IrPacketReceivedEvent(std::string message)
            : message(message)
        {
        }
        events::EventType type()
        {
            return IR_PACKET_RECEIVED_EVENT;
        }
        std::string message;
    };

    struct IrMessageReceivedEvent : public events::Event
    {
        IrMessageReceivedEvent(std::string message)
            : message(message)
        {
        }
        events::EventType type()
        {
            return IR_MESSAGE_RECEIVED_EVENT;
        }
        std::string message;
    };

    struct RequestIrMessageSendEvent : public events::Event
    {
        RequestIrMessageSendEvent(std::string message)
            : message(message)
        {
        }
        events::EventType type()
        {
            return REQUEST_IR_MESSAGE_SEND_EVENT;
        }
        std::string message;
    };

    struct IrBeaconReceivedEvent : public events::Event
    {
        IrBeaconReceivedEvent(int data)
            : data(data)
        {
        }
        events::EventType type()
        {
            return IR_BEACON_RECEIVED_EVENT;
        }
        int data;
    };
}
