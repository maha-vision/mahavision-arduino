#pragma once

#include <Application.hpp>

#define DEVICE_INFO_ID_INDEX 0
#define DEVICE_INFO_USER_NAME_INDEX 1
#define DEVICE_INFO_MAX_INDEX 3

class DeviceInfoApplication : public Application
{

public:
    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();
    void nextDeviceInfo();
    void previousDeviceInfo();
    static void drawPreview(display::LcdPtr);
    static ApplicationPtr create();

    friend class RerenderOnBatteryChangeEventHandler;
    friend class ChangeDeviceInfoOnSwipeEventHandler;

private:
    void displayDeviceInfo();
    uint8_t deviceInfoIndex = DEVICE_INFO_ID_INDEX;
	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
    std::vector<events::SubscriptionInfo> subscriptionInfos = {};
};
