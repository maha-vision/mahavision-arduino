#include "DeviceInfoApplication.hpp"
#include <Lcd.hpp>

#include <ButtonsEvents.hpp>
#include <BatteryEvents.hpp>
#include <Info.hpp>
#include <Config.hpp>


class RerenderOnBatteryChangeEventHandler : public events::EventHandler
{
public:
    RerenderOnBatteryChangeEventHandler(DeviceInfoApplication& deviceInfoApp)
    : deviceInfoApp(deviceInfoApp)
    {

    }
    void handle(events::EventPtr event)
    {
        logging::debug("in RerenderOnBatteryChangeEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<battery::BatteryChangedEvent>(event);
        deviceInfoApp.lcd->render();
    }
private:
    DeviceInfoApplication& deviceInfoApp;
};

class ChangeDeviceInfoOnSwipeEventHandler : public events::EventHandler
{
public:
	ChangeDeviceInfoOnSwipeEventHandler(DeviceInfoApplication& deviceInfoApp)
		:deviceInfoApp(deviceInfoApp)
	{

	}

    void handle(events::EventPtr event)
    {
    	logging::debug("in ChangeDeviceInfoOnSwipeEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsSwipedEvent>(event);
        if (realEvent->direction == buttons::Forward) {
            deviceInfoApp.nextDeviceInfo();
            logging::debug("ChangeDeviceInfoOnSwipeEventHandler next info\n");
        } else {
            deviceInfoApp.previousDeviceInfo();
            logging::debug("ChangeDeviceInfoOnSwipeEventHandler previous info\n");
        }
        
    }
private:
	DeviceInfoApplication& deviceInfoApp;
};

void DeviceInfoApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;
    
    logging::debug("Starting DeviceInfoApplication\n");
    displayDeviceInfo();
    
    subscriptionInfos.push_back(eventBus->subscribe(buttons::BTN_SWIPED_EVENT, std::make_shared<ChangeDeviceInfoOnSwipeEventHandler>(*this)));
    
    subscriptionInfos.push_back(eventBus->subscribe(battery::BATTERY_CHANGED_EVENT, std::make_shared<RerenderOnBatteryChangeEventHandler>(*this)));
}

void DeviceInfoApplication::finish()
{
    for (auto subscriptionInfo : subscriptionInfos)
    {
        eventBus->unsubscribe(subscriptionInfo);
    }
}

void DeviceInfoApplication::nextDeviceInfo()
{
    ++deviceInfoIndex;
    if (deviceInfoIndex == DEVICE_INFO_MAX_INDEX)
    {
        deviceInfoIndex = DEVICE_INFO_ID_INDEX;
    }

    displayDeviceInfo();
}

void DeviceInfoApplication::previousDeviceInfo()
{
    if (deviceInfoIndex != DEVICE_INFO_ID_INDEX)
    {
        --deviceInfoIndex;
        displayDeviceInfo();
    }
    else
    {
        deviceInfoIndex = DEVICE_INFO_MAX_INDEX - 1;
    }
}

void DeviceInfoApplication::displayDeviceInfo()
{
    switch(deviceInfoIndex) {
        case DEVICE_INFO_ID_INDEX:
            lcd->erase();
            lcd->scroll3EnglishSentences("id", "(mac)", info::get_persistent("id"));
            lcd->render();
            break;
        case DEVICE_INFO_USER_NAME_INDEX:
            lcd->erase();
            lcd->draw3HebrewSentences("םש", info::get_persistent("firstName"), info::get_persistent("lastName"));
            lcd->render();
            break;
    }
}

void DeviceInfoApplication::drawPreview(display::LcdPtr lcd)
{
    logging::debug("DeviceInfoApplication::drawPreview\n");
    lcd->erase();
    lcd->draw2HebrewSentences("עדימ", "רישכמ");
    lcd->render();
}

ApplicationPtr DeviceInfoApplication::create()
{
    return std::make_shared<DeviceInfoApplication>();
}
