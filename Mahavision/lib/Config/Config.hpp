#pragma once

#include <string>
#include <map>
#include "BleEvents.hpp"

// constants...

namespace config
{
	//preferences
	static const std::string preferencesNamespaceName = "mahavision";

	//info
	static const std::string infoServiceUuid = "INFO";

	static const std::map<std::string, std::string> infoDefaultValues = std::map<std::string, std::string>({
		std::make_pair("firstName", "ינולפ"),
    	std::make_pair("lastName", "ינומלא"),
		std::make_pair("id", "00000000"),
		std::make_pair("macAddress", "-1"),
		std::make_pair("eggIds", "[]"),
		std::make_pair("friends", "[]")
	});

	static const std::map<std::string, std::pair<std::string, ble::BleCharateristicProperties>> infoCharacteristics = std::map<std::string, std::pair<std::string, ble::BleCharateristicProperties>>({
		std::make_pair("FIRST0NAME000000", std::make_pair("firstName", ble::RW)),
    	std::make_pair("LAST0NAME0000000", std::make_pair("lastName", ble::RW)),
		std::make_pair("idid", std::make_pair("id", ble::R))
	});

	//easter egg app
	static const std::string easterEggServiceUuid = "EASTER0SERVICE00";
	static const std::string easterEggCharacteristcUuid = "EASTER0EGG0CHAR0";

	//friends app
	static const std::string friendsServiceUuid = "FRIENDS0SERVICE0";
	static const std::string friendsCharacteristcUuid = "FRIENDS0CHAR0000";

	//button held times
	static const int minimumHeldTimeForBoot = 4000;
	static const int minimumHeldTimeForMenu = 750;
}
