#pragma once

#include "FS.h"
#include <string>

struct filesystem {
    static void initialize();
    static File open(const std::string& name, const std::string& mode = FILE_READ);
    static bool exists(const std::string& path);
    static bool remove(const std::string& path);
    static bool rename(const std::string& pathFrom, const std::string& pathTo);
    static bool mkdir(const std::string& path);
    static bool rmdir(const std::string& path);
};
