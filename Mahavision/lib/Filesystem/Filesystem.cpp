#include "Filesystem.hpp"
#include "SPIFFS.h"

void filesystem::initialize()
{
    // first argument is formatOnFail
    SPIFFS.begin(true);
    //@todesign: throw exception if does not work
}

File filesystem::open(const std::string& name, const std::string& mode)
{
    SPIFFS.open(name.c_str(), mode.c_str());
}

bool filesystem::exists(const std::string& path)
{
    SPIFFS.exists(path.c_str());
}

bool filesystem::remove(const std::string& path)
{
    SPIFFS.remove(path.c_str());
}

bool filesystem::rename(const std::string& pathFrom, const std::string& pathTo)
{
    SPIFFS.rename(pathFrom.c_str(), pathTo.c_str());
}

bool filesystem::mkdir(const std::string& path)
{
    SPIFFS.mkdir(path.c_str());
}

bool filesystem::rmdir(const std::string& path)
{
    SPIFFS.rmdir(path.c_str());
}
