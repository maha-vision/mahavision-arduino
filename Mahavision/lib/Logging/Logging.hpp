#pragma once

enum LogLevel { Debug, Info, Warn, Error, Fatal };

namespace logging
{
    void initialize(LogLevel logLevel);
    void debug(const char *format_string, ...);
    void info(const char *format_string, ...);
    void warn(const char *format_string, ...);
    void error(const char *format_string, ...);
    void fatal(const char *format_string, ...);
    void log(LogLevel logLevel, const char *format_string, ...);
}
