#include "Logging.hpp"
#include <Arduino.h>

static LogLevel gLogLevel = Info;

static const int LOG_BUFFER_SIZE = 1000;

void logging::initialize(LogLevel logLevel = Info)
{
    Serial.begin(115200);
    gLogLevel = logLevel;
}

static void log_(LogLevel logLevel, const char* format_string, va_list args)
{
    if (logLevel >= gLogLevel)
    {
        // @todo: better buffer allocation (not on stack, compute size)
        char s[LOG_BUFFER_SIZE];
        vsprintf(s, format_string, args);
        Serial.print(s);
    }
}

void logging::debug(const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(Debug, format_string, args);
    va_end(args);
}

void logging::info(const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(Info, format_string, args);
    va_end(args);
}

void logging::warn(const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(Warn, format_string, args);
    va_end(args);
}

void logging::error(const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(Error, format_string, args);
    va_end(args);
}

void logging::fatal(const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(Fatal, format_string, args);
    va_end(args);
}

void log(LogLevel logLevel, const char *format_string, ...)
{
    va_list args;
    va_start(args, format_string);
    log_(logLevel, format_string, args);
    va_end(args);
}
