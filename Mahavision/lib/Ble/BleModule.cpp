#include "BleModule.hpp"
#include "BleEvents.hpp"
#include <Logging.hpp>
#include <Config.hpp>
#include <Info.hpp>
#include "esp_system.h"

ble::BleModule::BleModule()
    : server(nullptr),
    services(),
    characteristics(),
    eventBus(nullptr)
{    
}

namespace ble
{
    class RequestBleServiceCreateEventHandler : public events::EventHandler
    {
    public:
        RequestBleServiceCreateEventHandler(ble::BleModule& bleModule)
            :bleModule(bleModule)
        {

        }

        void handle(events::EventPtr event)
        {
            auto realEvent = std::static_pointer_cast<ble::RequestBleServiceCreateEvent>(event);
            bleModule.createService(realEvent->uuid);
        }

    private:
        ble::BleModule& bleModule;
    };

    class RequestBleServiceStartEventHandler : public events::EventHandler
    {
    public:
        RequestBleServiceStartEventHandler(ble::BleModule& bleModule)
            :bleModule(bleModule)
        {

        }

        void handle(events::EventPtr event)
        {
            auto realEvent = std::static_pointer_cast<ble::RequestBleServiceStartEvent>(event);
            bleModule.startService(realEvent->uuid);
        }

    private:
        ble::BleModule& bleModule;
    };

    class RequestBleCharacteristicCreateEventHandler : public events::EventHandler
    {
    public:
        RequestBleCharacteristicCreateEventHandler(ble::BleModule& bleModule)
            :bleModule(bleModule)
        {
        }

        void handle(events::EventPtr event)
        {
            auto realEvent = std::static_pointer_cast<ble::RequestBleCharacteristicCreateEvent>(event);
            bleModule.createCharacteristic(realEvent->serviceUuid, realEvent->characteristicUuid,
                realEvent->properties, realEvent->initialData);
        }

    private:
        ble::BleModule& bleModule;
    };

    class RequestBleCharacteristicUpdateEventHandler : public events::EventHandler
    {
    public:
        RequestBleCharacteristicUpdateEventHandler(ble::BleModule& bleModule)
            :bleModule(bleModule)
        {
        }

        void handle(events::EventPtr event)
        {
            auto realEvent = std::static_pointer_cast<ble::RequestBleCharacteristicUpdateEvent>(event);
            bleModule.updateCharacteristic(realEvent->characteristicUuid, realEvent->data);
        }

    private:
        ble::BleModule& bleModule;
    };
}

void ble::BleModule::createService(std::string serviceUuid)
{
    if (services.find(serviceUuid) != services.end())
    {
        return;
    }

    BLEService* service = server->createService(serviceUuid);
    services[serviceUuid] = ble::BleService(service, serviceUuid);
}

void ble::BleModule::startService(std::string serviceUuid)
{
    if (services.find(serviceUuid) == services.end())
    {
        return;
    }

    if (services[serviceUuid].started)
    {
        return;
    }

    services[serviceUuid].rawService->start();
    services[serviceUuid].started = true;
}


void ble::BleModule::createCharacteristic(std::string serviceUuid, std::string characteristicUuid, ble::BleCharateristicProperties properties, std::string initialData)
{
    if (characteristics.find(characteristicUuid) != characteristics.end())
    {
        return;
    }

    ble::BleService service = services[serviceUuid];
    uint32_t rawProperties = BLECharacteristic::PROPERTY_READ;
    if (properties == RW)
    {
        rawProperties = BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_WRITE;
    } 
    else if (properties == R)
    {
        rawProperties = BLECharacteristic::PROPERTY_READ;
    }
    BLECharacteristic* characteristic = service.rawService->createCharacteristic(
        characteristicUuid,
        rawProperties);

    characteristic->setValue(initialData);
    characteristics[characteristicUuid] = ble::BleCharacteristic(characteristic, characteristicUuid, initialData);
}

void ble::BleModule::updateCharacteristic(std::string characteristicUuid, std::string data)
{
    if (characteristics.find(characteristicUuid) == characteristics.end())
    {
        return;
    }

    characteristics[characteristicUuid].rawCharacteristic->setValue(data);
}

template <size_t N> 
std::string byteArrayToString(std::array<byte, N> array)
{
    char hexstr[array.size() * 2 + 1];
    int i;
    for (i=0; i<array.size(); i++) {
        sprintf(hexstr+i*2, "%02x", array[i]);
    }
    hexstr[i*2] = 0;
    std::string result = std::string(hexstr);
    return result;

}

void ble::BleModule::begin(events::EventBusPtr eventBus_)
{
    eventBus = eventBus_;

    std::string macAddress = info::get_persistent("macAddress");
    if (macAddress == "-1")
    {   
        //byte mac[6];
        std::array<byte, 6> mac;
        esp_read_mac(mac.begin(), ESP_MAC_WIFI_STA);
    
        macAddress = byteArrayToString(mac);
    
        info::set_persistent("id", macAddress);
        info::set_persistent("macAddress", macAddress);
    }

    std::string bleName = "mahavision-" + macAddress;

    logging::info("bleName is %s\n", bleName.c_str());

    BLEDevice::init(bleName);
    server = BLEDevice::createServer();
    advertising = server->getAdvertising();
    advertising->start();

    eventBus->subscribe(ble::REQUEST_BLE_SERIVCE_CREATE_EVENT, std::make_shared<ble::RequestBleServiceCreateEventHandler>(*this));
    eventBus->subscribe(ble::REQUEST_BLE_SERIVCE_START_EVENT, std::make_shared<ble::RequestBleServiceStartEventHandler>(*this));
    eventBus->subscribe(ble::REQUEST_BLE_CHARACTERISTIC_CREATE_EVENT, std::make_shared<ble::RequestBleCharacteristicCreateEventHandler>(*this));
    eventBus->subscribe(ble::REQUEST_BLE_CHARACTERISTIC_UPDATE_EVENT, std::make_shared<ble::RequestBleCharacteristicUpdateEventHandler>(*this));

    createService(config::infoServiceUuid);

    for (auto it = config::infoCharacteristics.begin(); it != config::infoCharacteristics.end(); ++it) {
        createCharacteristic(config::infoServiceUuid, it->first,
        it->second.second, info::get_persistent(it->second.first));
    }

    startService(config::infoServiceUuid);
}

void ble::BleModule::update()
{
    //@todesign: we could have used ble characteristic callbacks, but for some reasons, it caused bugs.
    for (std::map<std::string, ble::BleCharacteristic>::iterator it = characteristics.begin(); it != characteristics.end(); ++it) {
        ble::BleCharacteristic characteristic = it->second;
        if (characteristic.rawCharacteristic->getValue() != characteristic.lastValue)
        {
            characteristic.lastValue = characteristic.rawCharacteristic->getValue();

            if (config::infoCharacteristics.find(characteristic.uuid) != config::infoCharacteristics.end())
            {
                // logging::debug("setting %s : %s\n", it->first.c_str(), characteristic.lastValue.c_str());
                //@todo: handle exception
                info::set_persistent(config::infoCharacteristics.at(characteristic.uuid).first, characteristic.lastValue);
            }

            eventBus->publish(std::make_shared<BleCharacteristicChangedEvent>
                (characteristic.rawCharacteristic->getUUID().toString(), characteristic.lastValue));
        }
    }
}
