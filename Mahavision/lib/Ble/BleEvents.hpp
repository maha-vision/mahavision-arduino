#pragma once

#include <Event.hpp>
#include <string>

namespace ble {

    const events::EventType REQUEST_BLE_SERIVCE_CREATE_EVENT = (events::EventType)3000;
    const events::EventType REQUEST_BLE_SERIVCE_START_EVENT = (events::EventType)3001;
    const events::EventType REQUEST_BLE_CHARACTERISTIC_CREATE_EVENT = (events::EventType)3002;
    const events::EventType REQUEST_BLE_CHARACTERISTIC_UPDATE_EVENT = (events::EventType)3003;
    const events::EventType BLE_CHARACTERISTIC_CHANGED_EVENT = (events::EventType)3004;

    enum BleCharateristicProperties
    {
        RW,
        R
    };
    
    //@todesign: a bit ugly, should filter on uuid somehow
    struct BleCharacteristicChangedEvent : public events::Event
    {
        BleCharacteristicChangedEvent(std::string uuid, std::string data)
            : uuid(uuid), data(data)
        {
        }
        events::EventType type()
        {
            return BLE_CHARACTERISTIC_CHANGED_EVENT;
        }
        std::string uuid;
        std::string data;
    };

    struct RequestBleServiceCreateEvent : public events::Event
    {
        RequestBleServiceCreateEvent(std::string uuid)
            : uuid(uuid)
        {
        }
        events::EventType type()
        {
            return REQUEST_BLE_SERIVCE_CREATE_EVENT;
        }
        std::string uuid;
    };

    struct RequestBleServiceStartEvent : public events::Event
    {
        RequestBleServiceStartEvent(std::string uuid)
            : uuid(uuid)
        {
        }
        events::EventType type()
        {
            return REQUEST_BLE_SERIVCE_START_EVENT;
        }
        std::string uuid;
    };

    struct RequestBleCharacteristicCreateEvent : public events::Event
    {
        RequestBleCharacteristicCreateEvent(std::string serviceUuid, std::string characteristicUuid, BleCharateristicProperties properties, std::string initialData)
            : serviceUuid(serviceUuid), characteristicUuid(characteristicUuid), properties(properties), initialData(initialData)
        {
        }
        events::EventType type()
        {
            return REQUEST_BLE_CHARACTERISTIC_CREATE_EVENT;
        }
        std::string serviceUuid;
        std::string characteristicUuid;
        BleCharateristicProperties properties;
        std::string initialData;
    };

    struct RequestBleCharacteristicUpdateEvent : public events::Event
    {
        RequestBleCharacteristicUpdateEvent(std::string characteristicUuid, std::string data)
            : characteristicUuid(characteristicUuid), data(data)
        {
        }
        events::EventType type()
        {
            return REQUEST_BLE_CHARACTERISTIC_UPDATE_EVENT;
        }
        std::string characteristicUuid;
        std::string data;
    };
}
