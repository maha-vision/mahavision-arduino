#include <Arduino.h>
#include <Module.hpp>
#include <EventBus.hpp>
#include <map>
#include <vector>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include "BleEvents.hpp"


namespace ble
{
    struct BleCharacteristic
    {
        //@todo:?
        BleCharacteristic()
            :rawCharacteristic(nullptr),
            uuid(""),
            lastValue("")
        {

        }
        BleCharacteristic(BLECharacteristic* rawCharacteristic, std::string uuid, std::string lastValue)
            :rawCharacteristic(rawCharacteristic), uuid(uuid), lastValue(lastValue)
        {
        }
        BLECharacteristic* rawCharacteristic;
        std::string uuid;
        std::string lastValue;
    };

    struct BleService
    {
       //@todo:?
        BleService()
            :rawService(nullptr),
            uuid(""),
            started(false)
        {

        }

        BleService(BLEService* rawService, std::string uuid)
            :rawService(rawService), uuid(uuid), started(false)
        {
        }
        BLEService* rawService;
        std::string uuid;
        bool started;
    };

    class BleModule : public Module {
    public:
        BleModule();

        void begin(events::EventBusPtr eventBus);

        void update();

        friend class RequestBleServiceCreateEventHandler;
        friend class RequestBleServiceStartEventHandler;
        friend class RequestBleCharacteristicCreateEventHandler;
        friend class RequestBleCharacteristicUpdateEventHandler;
        friend class RequestBleStartEventHandler;
        
    private:

        void createService(std::string serviceUuid);
        void startService(std::string serviceUuid);

        void createCharacteristic(std::string serviceUuid, std::string characteristicUuid,
            ble::BleCharateristicProperties properties, std::string initialData);
        void updateCharacteristic(std::string characteristicUuid, std::string data);
        
        BLEServer* server;
        BLEAdvertising* advertising;
        std::map<std::string, ble::BleService> services;
        std::map<std::string, ble::BleCharacteristic> characteristics;
        events::EventBusPtr eventBus;
    };
}
