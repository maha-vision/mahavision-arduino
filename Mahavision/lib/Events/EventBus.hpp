#pragma once

#include "Event.hpp"
#include "EventHandler.hpp"
#include <map>
#include <tuple>
#include <memory>
#include "mutex.h"
#include <Logging.hpp>
#include <functional>

namespace events {

    typedef int SubscriptionId;

    struct SubscriptionInfo
    {
        SubscriptionInfo(events::EventType type, events::SubscriptionId id)
            :type(type), id(id)
        {

        }
        events::EventType type;
        events::SubscriptionId id;   
    };

    typedef std::map<SubscriptionId, EventHandlerPtr> SpecificSubscriptionMap;
    typedef std::map<EventType, SpecificSubscriptionMap> SubscriptionMap;

    class EventBus {
    public:
        EventBus();
        ~EventBus();
        void publish(EventPtr event);
        SubscriptionInfo subscribe(EventType type, EventHandlerPtr handler);
        void unsubscribe(SubscriptionInfo info);

    private:

        void lock();
        void unlock();

        std::map<EventType, SubscriptionId> subscriptionIdsCounter;
        SubscriptionMap subscriptions;
        
        // SemaphoreHandle_t xSemaphore; // @todo concurrency
    };

    typedef std::shared_ptr<EventBus> EventBusPtr;
}
