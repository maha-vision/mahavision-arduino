#pragma once

#include "Event.hpp"
#include <memory>

namespace events {

    class EventHandler
    {
    public:
        virtual ~EventHandler() = default;
        virtual void handle(EventPtr event) = 0;
    };

    typedef std::shared_ptr<EventHandler> EventHandlerPtr;

}
