#pragma once

#include <memory>

namespace events {

    typedef int EventType;

    struct Event {
        virtual ~Event() = default;
        virtual EventType type() = 0;
    };

    typedef std::shared_ptr<Event> EventPtr;
}
