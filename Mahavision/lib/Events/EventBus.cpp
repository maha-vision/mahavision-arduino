#include "EventBus.hpp"
#include <Arduino.h>
#include <mutex.h>
#include <Logging.hpp>

//@todesign: create mutex class that encapsulates the usage of xSemphore

events::EventBus::EventBus()
    : subscriptionIdsCounter(),
    subscriptions()
    // , xSemaphore(xSemaphoreCreateMutex()) // @todo concurrency
{
}

events::EventBus::~EventBus() 
{
    // vSemaphoreDelete(xSemaphore); // @todo concurrency
}

void events::EventBus::lock()
{
    // @todo concurrency:
    // if (xSemaphoreTake(xSemaphore, (TickType_t)500) != pdTRUE)
    // {
    //     //@todo concurrency:
    //    logging::fatal("This should not happen\n");
    // }
}

void events::EventBus::unlock()
{
    // @todo concurrency:
    // xSemaphoreGive(xSemaphore);
}

void events::EventBus::publish(events::EventPtr event)
{
    lock();

    events::EventType type = event->type();

    if (subscriptions.find(type) != subscriptions.end())
    {
        SpecificSubscriptionMap& map = subscriptions[type];
        for (SpecificSubscriptionMap::iterator it = map.begin(); it != map.end(); ++it) {
            it->second->handle(event);
        }
    }

    unlock();
}

events::SubscriptionInfo events::EventBus::subscribe(EventType type, EventHandlerPtr handler)
{
    lock();

    SubscriptionId newId = subscriptionIdsCounter[type];
    subscriptionIdsCounter[type] += 1;

    subscriptions[type][newId] = handler;

    unlock();

    return SubscriptionInfo(type, newId);
}

void events::EventBus::unsubscribe(SubscriptionInfo info)
{
    lock();

    subscriptions[info.type].erase(info.id);

    unlock();
}
