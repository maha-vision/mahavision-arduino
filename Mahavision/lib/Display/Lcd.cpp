#include "Lcd.hpp"
#include <Logging.hpp>
#include <Info.hpp>
#include <sstream>
#include <algorithm>

//@todesign: these defines should be in the lcd configuration, but for some reason it does not work.

// Maybe define as 255?
#define U8G2_ESP32_HAL_UNDEFINED (-1)

// CLK - GPIO21 / D0
#define PIN_CLK 21

// MOSI - GPIO 22 / D1
#define PIN_MOSI 22

// RESET - GPIO 27
#define PIN_RESET 27

// DC - GPIO 4
#define PIN_DC 4

#define MAX_CHARACTERS_IN_LINE 6

const int TEXT_ANIMATION_DELAY = 100;

display::Lcd::Lcd()
	: U8G2_SSD1306_64X48_ER_F_4W_SW_SPI(U8G2_R0, /* clock=*/PIN_CLK, /* data=*/PIN_MOSI, /* cs=*/U8G2_ESP32_HAL_UNDEFINED, /* dc=*/PIN_DC, /* reset=*/PIN_RESET)
	, config(display::Lcd::LcdConfiguration())
{

}

display::Lcd::Lcd(display::Lcd::LcdConfiguration config)
	: U8G2_SSD1306_64X48_ER_F_4W_SW_SPI(U8G2_R0, /* clock=*/PIN_CLK, /* data=*/PIN_MOSI, /* cs=*/U8G2_ESP32_HAL_UNDEFINED, /* dc=*/PIN_DC, /* reset=*/PIN_RESET)
	, config(config)
	
{

}

void display::Lcd::drawAux(uint8_t x, uint8_t y, std::string str)
{
	drawUTF8(x, y, str.c_str());
}

void display::Lcd::drawHebrew(uint8_t x, uint8_t y, std::string str)
{
	int n = str.length() / 2 + str.length() % 2;
	if (n < 8)
	{
		for (size_t i = 0; i < 8 - n; ++i)
	    {
			str = " " + str;
	    }
	}
	setFont(config.hebrewFont);
	drawAux(x, y, str);
}

void display::Lcd::drawEnglish(uint8_t x, uint8_t y, std::string str)
{
	setFont(config.englishFont);
	drawAux(x, y, str);
}

void display::Lcd::erase()
{
	clearBuffer();
}

void display::Lcd::render()
{
	setFont(u8g2_font_5x7_tf);
	drawAux(45, 6, info::get("batteryPercentage") + "% ");
	sendBuffer();
}

void display::Lcd::draw1EnglishSentences(std::string sentence1)
{
	drawEnglish(0, 30, sentence1);
}
void display::Lcd::draw2EnglishSentences(std::string sentence1, std::string sentence2)
{
	drawEnglish(0, 20, sentence1);
	drawEnglish(0, 40, sentence2);
}
void display::Lcd::draw3EnglishSentences(std::string sentence1, std::string sentence2, std::string sentence3)
{
	drawEnglish(0, 19, sentence1);
	drawEnglish(0, 19 + 14, sentence2);
	drawEnglish(0, 19 + 14 + 14, sentence3);
}

void display::Lcd::draw1HebrewSentences(std::string sentence1)
{
	drawHebrew(0, 30, sentence1);
}
void display::Lcd::draw2HebrewSentences(std::string sentence1, std::string sentence2)
{
	drawHebrew(0, 20, sentence1);
	drawHebrew(0, 40, sentence2);
}
void display::Lcd::draw3HebrewSentences(std::string sentence1, std::string sentence2, std::string sentence3)
{
	drawHebrew(0, 19, sentence1);
	drawHebrew(0, 19 + 14, sentence2);
	drawHebrew(0, 19 + 14 + 14, sentence3);
}

void display::Lcd::run1EnglishSentencesAnimation(std::string sentence1)
{
	for (int i = 0 ; i < sentence1.length() ; i++)
	{
		draw1EnglishSentences(sentence1.substr(0, i + 1));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}
void display::Lcd::run2EnglishSentencesAnimation(std::string sentence1, std::string sentence2)
{
	for (int i = 0 ; i < sentence1.length() ; i++)
	{
		draw2EnglishSentences(sentence1.substr(0, i + 1), "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = 0 ; i < sentence2.length() ; i++)
	{
		draw2EnglishSentences(sentence1, sentence2.substr(0, i + 1));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}
void display::Lcd::run3EnglishSentencesAnimation(std::string sentence1, std::string sentence2, std::string sentence3)
{
	for (int i = 0 ; i < sentence1.length() ; i++)
	{
		draw3EnglishSentences(sentence1.substr(0, i + 1), "", "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = 0 ; i < sentence2.length() ; i++)
	{
		draw3EnglishSentences(sentence1, sentence2.substr(0, i + 1), "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = 0 ; i < sentence3.length() ; i++)
	{
		draw3EnglishSentences(sentence1, sentence2, sentence3.substr(0, i + 1));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}

void display::Lcd::run1HebrewSentencesAnimation(std::string sentence1)
{
	for (int i = sentence1.length() ; i > -1 ; i = i - 2)
	{
		draw1HebrewSentences(sentence1.substr(i, sentence1.length()));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}
void display::Lcd::run2HebrewSentencesAnimation(std::string sentence1, std::string sentence2)
{
	for (int i = sentence1.length() ; i > -1 ; i = i - 2)
	{
		draw2HebrewSentences(sentence1.substr(i, sentence1.length()), "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = sentence2.length() ; i > -1 ; i = i - 2)
	{
		draw2HebrewSentences(sentence1, sentence2.substr(i, sentence2.length()));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}
void display::Lcd::run3HebrewSentencesAnimation(std::string sentence1, std::string sentence2, std::string sentence3)
{
	for (int i = sentence1.length() ; i > -1 ; i = i - 2)
	{
		draw3HebrewSentences(sentence1.substr(i, sentence1.length()), "", "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = sentence2.length() ; i > -1 ; i = i - 2)
	{
		draw3HebrewSentences(sentence1, sentence2.substr(i, sentence2.length()), "");
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
	for (int i = sentence3.length() ; i > -1 ; i = i - 2)
	{
		draw3HebrewSentences(sentence1, sentence2, sentence3.substr(i, sentence3.length()));
		render();
		delay(TEXT_ANIMATION_DELAY);
	}
}

void display::Lcd::scroll3EnglishSentences(std::string sentence1, std::string sentence2, std::string sentence3)
{
	auto maxSize = std::max({sentence1.length(), sentence2.length(), sentence3.length()});
	if (maxSize < MAX_CHARACTERS_IN_LINE) 
	{
		draw3EnglishSentences(sentence1, sentence2, sentence3);
	}
	else
	{
		draw3EnglishSentences(sentence1, sentence2, sentence3);
		delay(TEXT_ANIMATION_DELAY * 5);

		for (auto i = 1u; i < maxSize - MAX_CHARACTERS_IN_LINE; ++i)
		{
			auto index1 = std::min(i, ((sentence1.length() > MAX_CHARACTERS_IN_LINE) ? (sentence1.length() - MAX_CHARACTERS_IN_LINE) : 0));
			auto index2 = std::min(i, ((sentence2.length() > MAX_CHARACTERS_IN_LINE) ? (sentence2.length() - MAX_CHARACTERS_IN_LINE) : 0));
			auto index3 = std::min(i, ((sentence3.length() > MAX_CHARACTERS_IN_LINE) ? (sentence3.length() - MAX_CHARACTERS_IN_LINE): 0));

			erase();
			draw3EnglishSentences(sentence1.substr(index1), sentence2.substr(index2), sentence3.substr(index3));
			render();
			delay(TEXT_ANIMATION_DELAY * 5);
		}
	}
	
}

void display::Lcd::runXBMAnimation(const LcdAnimation & lcdAnimation)
{
	for (uint8_t repeatCount = 0; repeatCount < lcdAnimation.repeatCount; ++repeatCount)
	{
		for(auto& frame: lcdAnimation.frames) {
			drawXBM(
				lcdAnimation.x,
				lcdAnimation.y,
				lcdAnimation.width,
				lcdAnimation.hight,
				frame.data());
			render();
			delay(lcdAnimation.frameDelay);
		}
	}
	erase();
}
