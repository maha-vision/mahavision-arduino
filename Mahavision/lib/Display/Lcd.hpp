#pragma once

#include <vector>
#include <Arduino.h>
#include <U8g2lib.h>
#include <string>
#include <memory>

#ifdef U8X8_HAVE_HW_SPI
#include <SPI.h>
#endif
#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

namespace display
{
    class Lcd : public U8G2_SSD1306_64X48_ER_F_4W_SW_SPI
    {
	public:
		struct LcdConfiguration
		{
			const uint8_t* hebrewFont = u8g2_font_unifont_t_hebrew;
			const uint8_t* englishFont = u8g2_font_unifont_t_hebrew;
		};

		struct LcdAnimation
		{
			const uint8_t x;
			const uint8_t y;
			const uint8_t hight;
			const uint8_t width;
			uint8_t repeatCount;
			uint32_t frameDelay;
			const std::vector<std::vector<uint8_t>> frames;
		};

		Lcd();
		Lcd(LcdConfiguration config);

		void run1EnglishSentencesAnimation(std::string);
		void run2EnglishSentencesAnimation(std::string, std::string);
		void run3EnglishSentencesAnimation(std::string, std::string, std::string);

		void run1HebrewSentencesAnimation(std::string);
		void run2HebrewSentencesAnimation(std::string, std::string);
		void run3HebrewSentencesAnimation(std::string, std::string, std::string);

		void runXBMAnimation(const LcdAnimation & lcdAnimation);

		void drawHebrew(uint8_t x, uint8_t y, std::string str);
		void drawEnglish(uint8_t x, uint8_t y, std::string str);

		void draw1EnglishSentences(std::string);
		void draw2EnglishSentences(std::string, std::string);
		void draw3EnglishSentences(std::string, std::string, std::string);

		void draw1HebrewSentences(std::string);
		void draw2HebrewSentences(std::string, std::string);
		void draw3HebrewSentences(std::string, std::string, std::string);

		void scroll3EnglishSentences(std::string sentence1, std::string sentence2, std::string sentence3);

		void erase();
		void render();

	private:
		void drawAux(uint8_t x, uint8_t y, std::string str);

		LcdConfiguration config;
    };


    typedef std::shared_ptr<Lcd> LcdPtr;
}
