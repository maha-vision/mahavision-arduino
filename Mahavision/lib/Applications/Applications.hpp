#pragma once

#include <tuple>
#include <Application.hpp>

// Applications
#include <DemoApplication.hpp>
#include <EasterEggApplication.hpp>
#include <FriendsApplication.hpp>
#include <RebootApplication.hpp>
#include <FactoryResetApplication.hpp>
#include <DeviceInfoApplication.hpp>

static const std::vector<std::tuple<DrawApplicationPreview, CreateApplication>> APPlLICATIONS = 
{
	//std::make_tuple<DrawApplicationPreview, CreateApplication>(DemoApplication::drawPreview, DemoApplication::create),
	std::make_tuple<DrawApplicationPreview, CreateApplication>(EasterEggApplication::drawPreview, EasterEggApplication::create),
	std::make_tuple<DrawApplicationPreview, CreateApplication>(FriendsApplication::drawPreview, FriendsApplication::create),
	std::make_tuple<DrawApplicationPreview, CreateApplication>(RebootApplication::drawPreview, RebootApplication::create),
	std::make_tuple<DrawApplicationPreview, CreateApplication>(FactoryResetApplication::drawPreview, FactoryResetApplication::create),
	std::make_tuple<DrawApplicationPreview, CreateApplication>(DeviceInfoApplication::drawPreview, DeviceInfoApplication::create)
};