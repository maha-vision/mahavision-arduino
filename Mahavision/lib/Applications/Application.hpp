#pragma once

#include <EventBus.hpp>
#include <Lcd.hpp>
#include <memory>

/*
Model : Application
View  : Lcd
Controller: EventBus
*/

class Application
{
public:
    virtual ~Application() = default;
    virtual void begin(events::EventBusPtr, display::LcdPtr) = 0;
    virtual void finish() = 0;
};

typedef std::shared_ptr<Application> ApplicationPtr;

typedef void (*DrawApplicationPreview)(display::LcdPtr);
typedef ApplicationPtr (*CreateApplication)();