#include "RebootApplication.hpp"

#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
#include <Config.hpp>

// Events
#include <ButtonsEvents.hpp>


void RebootApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;

    lcd->erase();
    lcd->draw2HebrewSentences("לחתאמ", "שדחמ");
    lcd->render();

    delay(1000);
    lcd->erase();
    ESP.restart();

}

void RebootApplication::finish()
{
}

void RebootApplication::drawPreview(display::LcdPtr lcd)
{
    lcd->erase();
    lcd->draw2HebrewSentences("לחתא", "שדחמ");
    lcd->render();
}

ApplicationPtr RebootApplication::create()
{
    return std::make_shared<RebootApplication>();
}
