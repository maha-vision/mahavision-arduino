#pragma once

#include <Application.hpp>


class RebootApplication : public Application
{
public:
    void begin(events::EventBusPtr, display::LcdPtr);
	void finish();

    static void drawPreview(display::LcdPtr);
    static ApplicationPtr create();

private:
	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
};
