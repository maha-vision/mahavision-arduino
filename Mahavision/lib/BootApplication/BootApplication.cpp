#include "BootApplication.hpp"
#include "BootAnimation.hpp"
#include <ButtonsEvents.hpp>
#include <BatteryEvents.hpp>
#include <Info.hpp>
#include <Config.hpp>

class ShutdownOnLowBatteryEventHandler : public events::EventHandler
{
public:
    ShutdownOnLowBatteryEventHandler(BootApplication& bootApp)
    : bootApp(bootApp)
    {

    }
    void handle(events::EventPtr event)
    {
        logging::debug("in ShutdownOnLowBatteryEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<battery::BatteryChangedEvent>(event);
        if (realEvent->batteryChargePrecent < 10)
        {
            bootApp.shutdown();
        }
    }
private:
    BootApplication& bootApp;
};

class RerenderOnBatteryChangeEventHandler : public events::EventHandler
{
public:
    RerenderOnBatteryChangeEventHandler(BootApplication& bootApp)
    : bootApp(bootApp)
    {

    }
    void handle(events::EventPtr event)
    {
        logging::debug("in RerenderOnBatteryChangeEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<battery::BatteryChangedEvent>(event);
        bootApp.lcd->render();
    }
private:
    BootApplication& bootApp;
};

void BootApplication::shutdown()
{
    lcd->erase();
    lcd->drawEnglish(0, 30, "יוביכ");
    lcd->render();
    delay(1000);
    lcd->erase();
    ESP.deepSleep(999999999);
}

void BootApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;
    
    logging::debug("Starting bootApp\n");
    logging::debug("Running boot animation\n");
    lcd->runXBMAnimation(bootAnimation);
    logging::debug("Running welcome message animation\n");
    lcd->run3HebrewSentencesAnimation("םולש", info::get_persistent("firstName"), info::get_persistent("lastName"));
    delay(1000);
    
    //@todo: do we need this?
    //eventBus->subscribe(battery::BATTERY_CHANGED_EVENT, std::make_shared<ShutdownOnLowBatteryEventHandler>(*this));
    
    eventBus->subscribe(battery::BATTERY_CHANGED_EVENT, std::make_shared<RerenderOnBatteryChangeEventHandler>(*this));
}

void BootApplication::finish()
{
    
}
