#pragma once

#include <Application.hpp>

class BootApplication : public Application
{
public:
    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();

    friend class ShutdownOnLowBatteryEventHandler;
    friend class RerenderOnBatteryChangeEventHandler;
private:
	void shutdown();
	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
};
