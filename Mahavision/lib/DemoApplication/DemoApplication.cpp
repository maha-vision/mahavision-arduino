#include "DemoApplication.hpp"

#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>

// Events
#include <ButtonsEvents.hpp>
#include <IrEvents.hpp>
#include <BleEvents.hpp>

class ButtonsPressedEventHandler : public events::EventHandler
{
public:
    ButtonsPressedEventHandler(events::EventBusPtr eventBus)
        :eventBus(eventBus)
    {}

    void handle(events::EventPtr event)
    {
        // logging::debug("in ButtonsPressedEventHandler::handle\n");
        eventBus->publish(std::make_shared<ir::RequestIrMessageSendEvent>("hi"));
    }
private:
    events::EventBusPtr eventBus;
};

class ButtonsSwipedEventHandler : public events::EventHandler
{
public:
    ButtonsSwipedEventHandler(events::EventBusPtr eventBus)
        :eventBus(eventBus)
    {}
    void handle(events::EventPtr event)
    {
        // logging::debug("in ButtonsSwipedEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsSwipedEvent>(event);
    }
private:
    events::EventBusPtr eventBus;
};

class ButtonsHeldEventHandler : public events::EventHandler
{
public:
    ButtonsHeldEventHandler(std::shared_ptr<multithreading::Thread> thread)
        :thread(thread)
    {
    }

    void handle(events::EventPtr event)
    {
        // logging::debug("in ButtonsHeldEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsHeldEvent>(event);
        //thread.kill();
    }

private:
    std::shared_ptr<multithreading::Thread> thread;
};

void threadFunction(void *param) {
    //eventBus->publish(std::make_shared<ble::RequestBleServiceCreateEvent>("BABA"));
    while (1)
    {
        // logging::info("loop2 with param %d, firstame is %s\n", param, info::get_persistent("firstName").c_str());
        delay(1000);
    }
}

void DemoApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;

    thread = std::make_shared<multithreading::Thread>(threadFunction, (void*)7, 16000);

    // Custom subscriptions
    subscriptionInfos.push_back(eventBus->subscribe(buttons::BTN_PRESSED_EVENT, std::make_shared<ButtonsPressedEventHandler>(eventBus)));
    subscriptionInfos.push_back(eventBus->subscribe(buttons::BTN_SWIPED_EVENT, std::make_shared<ButtonsSwipedEventHandler>(eventBus)));
    subscriptionInfos.push_back(eventBus->subscribe(buttons::BTN_HELD_EVENT, std::make_shared<ButtonsHeldEventHandler>(thread)));
    
    // Custom publishes
    eventBus->publish(std::make_shared<ble::RequestBleServiceCreateEvent>("BLAT"));
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicCreateEvent>("BLAT", "DAAT", ble::RW, "HELLO WORLD"));
    eventBus->publish(std::make_shared<ble::RequestBleServiceStartEvent>("BLAT"));

    // Custom
    lcd->erase();
    lcd->drawEnglish(30, 30, "hello!");
    lcd->drawHebrew(10, 10, "שלום");
    lcd->drawCircle(40, 40, 5);
    lcd->render();
}

void DemoApplication::finish()
{
    for(auto subscriptionInfo : subscriptionInfos)
    {
        eventBus->unsubscribe(subscriptionInfo);
    }
    thread->kill();
}

void DemoApplication::drawPreview(display::LcdPtr lcd)
{
    lcd->erase();
    lcd->drawEnglish(0, 30, "Demo App");
    lcd->render();
}

ApplicationPtr DemoApplication::create()
{
    return std::make_shared<DemoApplication>();
}
