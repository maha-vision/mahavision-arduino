#pragma once

#include <Application.hpp>
#include <Thread.hpp>

class DemoApplication : public Application
{
public:
    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();

    static void drawPreview(display::LcdPtr);
    static ApplicationPtr create();

private:
	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
	std::shared_ptr<multithreading::Thread> thread = nullptr;
	std::vector<events::SubscriptionInfo> subscriptionInfos = {};
};
