#pragma once

#include <EventBus.hpp>
#include <memory>


class Module
{
public:
    virtual ~Module() = default;
    virtual void begin(events::EventBusPtr) = 0;
    virtual void update() = 0;
};

typedef std::shared_ptr<Module> ModulePtr;
