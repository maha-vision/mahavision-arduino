#include "EasterEggApplication.hpp"
#include "EasterEggAnimation.hpp"
#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
#include <Config.hpp>
#include <Info.hpp>

// Events
#include <ButtonsEvents.hpp>
#include <IrEvents.hpp>
#include <BleEvents.hpp>

#include <algorithm>
#include <sstream>


class EggCaughtEventHandler : public events::EventHandler
{
public:
    EggCaughtEventHandler(EasterEggApplication& easterEggApp)
        :easterEggApp(easterEggApp)
    {
    }

    void handle(events::EventPtr event)
    {
        auto realEvent = std::static_pointer_cast<ir::IrBeaconReceivedEvent>(event);
        EggId id(realEvent->data);
        easterEggApp.eggCaught(id);
    }

private:
    EasterEggApplication& easterEggApp;
};

void EasterEggApplication::eggCaught(EggId id)
{
    drawEasterEggAnimation();

    std::stringstream stream;
    stream << id;
    std::string eggIdAsString = stream.str();

    if (std::find(eggIds.begin(), eggIds.end(), id) != eggIds.end()) {
        lcd->erase();
        lcd->draw3HebrewSentences("ןמוקיפא", eggIdAsString, "בוש אצמנ");
        lcd->render();
    } else {
        lcd->erase();
        lcd->draw3HebrewSentences("ןמוקיפא", eggIdAsString, "!אצמנ");
        lcd->render();
        eggIds.push_back(id);
        sendEggIds();
    }

    delay(2000);
    drawGoodLuck();
}

void EasterEggApplication::drawGoodLuck()
{
    lcd->erase();
    lcd->draw2HebrewSentences("שפח", "!ןמוקיפא");
    lcd->render();
}

void EasterEggApplication::drawEasterEggAnimation()
{
    lcd->runXBMAnimation(easterEggAnimation);
    // lcd->erase();
    // lcd->draw2HebrewSentences("פפאחמ", "!אצמנ");
    // lcd->render();

    // delay(2000);
}

void EasterEggApplication::deserializeEggIds(std::string serializedEggIds)
{
    std::string innerValues = serializedEggIds.substr(1, serializedEggIds.length() - 2);

    std::stringstream ss(innerValues);

    int i;

    while (ss >> i)
    {
        eggIds.push_back(i);

        if (ss.peek() == ',')
            ss.ignore();
    }
}

std::string EasterEggApplication::serializeEggIds()
{
    if (eggIds.empty())
    {
        return "[]";
    }

    std::string serialized = "[";
    for(auto eggId : eggIds)
    {
        std::stringstream stream;
        stream << eggId;
        std::string eggIdAsString = stream.str();
        serialized += eggIdAsString + ",";
    }
    serialized.erase(serialized.size() - 1);
    serialized += "]";
    return serialized;
}

void EasterEggApplication::sendEggIds()
{
    std::string serializedEggIds = serializeEggIds();

    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicUpdateEvent>
        (config::easterEggCharacteristcUuid, serializedEggIds));

    info::set_persistent("eggIds", serializedEggIds);
}


void EasterEggApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;

    // Custom subscriptions
    subscriptionInfos.push_back(eventBus->subscribe(ir::IR_BEACON_RECEIVED_EVENT, std::make_shared<EggCaughtEventHandler>(*this)));

    eventBus->publish(std::make_shared<ble::RequestBleServiceCreateEvent>(config::easterEggServiceUuid));
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicCreateEvent>(config::easterEggServiceUuid, config::easterEggCharacteristcUuid, ble::R, serializeEggIds()));
    eventBus->publish(std::make_shared<ble::RequestBleServiceStartEvent>(config::easterEggServiceUuid));

    drawGoodLuck();

    std::string serializedEggIds = info::get_persistent("eggIds");

    deserializeEggIds(serializedEggIds);
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicUpdateEvent>
        (config::easterEggCharacteristcUuid, serializedEggIds));
}

void EasterEggApplication::finish()
{
    for (auto subscriptionInfo : subscriptionInfos)
    {
        eventBus->unsubscribe(subscriptionInfo);
    }
}

void EasterEggApplication::drawPreview(display::LcdPtr lcd)
{
    lcd->erase();
    lcd->draw2HebrewSentences("פפאחמ", "ןמוקיפא");
    lcd->render();
}

ApplicationPtr EasterEggApplication::create()
{
    return std::make_shared<EasterEggApplication>();
}
