#pragma once

#include <Application.hpp>
#include <Thread.hpp>

typedef int EggId;

class EasterEggApplication : public Application
{
public:
    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();

    static void drawPreview(display::LcdPtr);
    static ApplicationPtr create();

    friend class EggCaughtEventHandler;

private:
	void eggCaught(EggId eggId);
	void drawGoodLuck();
	void drawEasterEggAnimation();
	void deserializeEggIds(std::string serializedEggIds);
	std::string serializeEggIds();
	void sendEggIds();

	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
	std::vector<events::SubscriptionInfo> subscriptionInfos = {};
	std::vector<EggId> eggIds = {};
};
