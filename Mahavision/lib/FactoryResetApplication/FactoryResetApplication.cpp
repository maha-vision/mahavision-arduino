#include "FactoryResetApplication.hpp"

#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
#include <Config.hpp>

// Events
#include <ButtonsEvents.hpp>


void FactoryResetApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;

    lcd->erase();
    lcd->draw3HebrewSentences("לחתאמ", "רישכמ", "לעפמל");
    lcd->render();
    delay(1000);
    //@todesign: better way for doing this
    info::set_persistent("firstName", "ינולפ");
    info::set_persistent("lastName", "ינומלא");
    info::set_persistent("id", "00000000");
    info::set_persistent("macAddress", "-1");
    info::set_persistent("eggIds", "[]");
    info::set_persistent("friends", "[]");
    lcd->erase();
    ESP.restart();

}

void FactoryResetApplication::finish()
{
}

void FactoryResetApplication::drawPreview(display::LcdPtr lcd)
{
    lcd->erase();
    lcd->draw2HebrewSentences("לוחתא", "לעפמ");
    lcd->render();
}

ApplicationPtr FactoryResetApplication::create()
{
    return std::make_shared<FactoryResetApplication>();
}
