#include "EventBasedButton.hpp"
#include <BasicButton.h>
#include <Button.h>
#include <ButtonEventCallback.h>

EventBasedButton::EventBasedButtonConfiguration::EventBasedButtonConfiguration(uint8_t pin)
    : pin(pin)
{
}

// btn is a reference to the button that fired the event. That means you can use the same event handler for many buttons
void onButtonPressed(Button& btn)
{
    EventBasedButton& theBtn = static_cast<EventBasedButton&>(btn);
    theBtn.buttonEventHandler().onButtonPressed();
}

// duration reports back how long it has been since the button was originally pressed.
// repeatCount tells us how many times this function has been called by this button.
void onButtonHeld(Button& btn, uint16_t duration, uint16_t repeatCount)
{
    EventBasedButton& theBtn = static_cast<EventBasedButton&>(btn);
    theBtn.buttonEventHandler().onButtonHeld(duration, repeatCount);
}

// duration reports back the total time that the button was held down
void onButtonReleased(Button& btn, uint16_t duration)
{
    EventBasedButton& theBtn = static_cast<EventBasedButton&>(btn);
    theBtn.buttonEventHandler().onButtonReleased(duration);
}

EventBasedButton::EventBasedButton(EventBasedButtonConfiguration config, ButtonEventHandler& handler)
    : BasicButton(config.pin)
    , config(config)
    , handler(handler)
{
}

void EventBasedButton::begin()
{
    // When the button is first pressed, call the function onButtonPressed
    onPress(onButtonPressed);
    // Once the button has been held for config.onHoldDuration ms call onButtonHeld. Call it again every config.onHoldRepeatEvery ms until it is let go
    onHoldRepeat(config.onHoldDuration, config.onHoldRepeatEvery, onButtonHeld);
    // When the button is released, call onButtonReleased if the button was pressed for config.onReleaseWait ms and not more then config.onReleaseMaxWait ms
    onRelease(config.onReleaseWait, config.onReleaseMaxWait ,onButtonReleased);
}

ButtonEventHandler& EventBasedButton::buttonEventHandler()
{
    return handler;
}
