#pragma once

#include "ButtonEventHandler.hpp"
#include <BasicButton.h>
#include <Button.h>
#include <ButtonEventCallback.h>

//@todesign: add namespace?
//@todesign: folder name?

class EventBasedButton : public BasicButton {
public:
    struct EventBasedButtonConfiguration
    {
        EventBasedButtonConfiguration(uint8_t pin);
        uint8_t pin;
        uint16_t onReleaseWait = 0;
        uint16_t onReleaseMaxWait = -1;
        uint16_t onHoldDuration = 500;
        uint16_t onHoldRepeatEvery = 250;
    };

    EventBasedButton(EventBasedButtonConfiguration config, ButtonEventHandler& buttonEventHandler);

    void begin();

    ButtonEventHandler& buttonEventHandler();

private:
    EventBasedButtonConfiguration config;
    ButtonEventHandler& handler;
};
