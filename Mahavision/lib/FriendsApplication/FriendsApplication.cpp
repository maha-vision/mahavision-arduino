#include "FriendsApplication.hpp"
#include "FriendsIcon.hpp"

#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
#include <Config.hpp>

// Events
#include <ButtonsEvents.hpp>
#include <IrEvents.hpp>
#include <BleEvents.hpp>

#include <algorithm>
#include <sstream>


class FriendMetEventHandler : public events::EventHandler
{
public:
    FriendMetEventHandler(FriendsApplication& FriendsApp)
        :FriendsApp(FriendsApp)
    {
    }

    void handle(events::EventPtr event)
    {
        auto realEvent = std::static_pointer_cast<ir::IrMessageReceivedEvent>(event);
        logging::debug("found %s\n", realEvent->message.c_str());
        Friend afriend(realEvent->message);
        FriendsApp.friendMet(afriend);
    }

private:
    FriendsApplication& FriendsApp;
};

class ShouldSendInfoEventHandler : public events::EventHandler
{
public:
    ShouldSendInfoEventHandler(FriendsApplication& FriendsApp)
        :FriendsApp(FriendsApp)
    {

    }

    void handle(events::EventPtr event)
    {
        logging::debug("in ShouldSendInfoEventHandler::handle\n");
        auto realEvent = std::static_pointer_cast<buttons::ButtonsSwipedEvent>(event);
        if (realEvent->direction == buttons::Forward)
        {
            FriendsApp.sendInfo();
        }
    }
private:
    FriendsApplication& FriendsApp;
};

void FriendsApplication::friendMet(Friend afriend)
{
    drawFriendAnimation();

    //@todo: first name, second name must be in hebrew

    if (std::find(friends.begin(), friends.end(), afriend) != friends.end()) {
        lcd->erase();
        lcd->draw3HebrewSentences(afriend.firstName, afriend.lastName, "בוש אצמנ");
        lcd->render();
    } else {
        lcd->erase();
        lcd->draw3HebrewSentences("תא תשגפ", afriend.firstName, afriend.lastName);
        lcd->render();
        friends.push_back(afriend);
        sendLastFriends();
    }

    delay(2000);
    drawGoodLuck();
}

void FriendsApplication::drawGoodLuck()
{
    lcd->erase();
    lcd->draw2HebrewSentences("שפח", "!םירבח");
    lcd->render();
}

void FriendsApplication::drawFriendAnimation()
{
    //@todo: animation
    lcd->erase();
    lcd->draw2HebrewSentences("תאצמ", "!רבח");
    lcd->render();

    delay(2000);
}


void FriendsApplication::deserializeFriends(std::string serializedFriends)
{
    std::string innerValues = serializedFriends.substr(1, serializedFriends.length() - 2);

    auto prev = std::begin(innerValues);

    auto curr = std::find(prev, std::end(innerValues), ',');
    while (curr != std::end(innerValues))
    {
        std::string currentFriend = std::string(prev, curr);
        friends.push_back(Friend(currentFriend));

        prev = curr + 1;
        curr = std::find(prev, std::end(innerValues), ',');
    }
}

std::string FriendsApplication::serializeFriends()
{
    if (friends.empty())
    {
        return "[]";
    }

    std::string serialized = "[";
    
    for (std::vector<Friend>::size_type i = 0; i < friends.size(); i++) {
        serialized += friends[i].serialize() + ",";
    }

    serialized += "]";
    return serialized;
}

std::string FriendsApplication::serializeLastFriends()
{
    if (friends.empty())
    {
        return "[]";
    }

    std::string serialized = "[";
    

    for (std::vector<Friend>::size_type i = 0; i != friends.size() && i < N_LAST_FRIENDS; i++) {
        serialized += friends[friends.size() - i - 1].serialize() + ",";
    }

    serialized.erase(serialized.size() - 1);
    serialized += "]";
    return serialized;
}

void FriendsApplication::sendInfo()
{
    lcd->erase();
    lcd->draw2HebrewSentences("חלוש", "םיטרפ");
    lcd->render();

    delay(1000);

    logging::debug("going to create me\n");

    Friend me(info::get_persistent("firstName"), info::get_persistent("lastName"), info::get_persistent("id"));

    std::string str = me.serialize();
    logging::debug("str is %s\n", str.c_str());

    logging::debug("sending info!\n");
    eventBus->publish(std::make_shared<ir::RequestIrMessageSendEvent>(str));

    drawGoodLuck();
}

void FriendsApplication::sendLastFriends()
{
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicUpdateEvent>
        (config::friendsCharacteristcUuid, serializeLastFriends()));

    info::set_persistent("friends", serializeFriends());
}

void FriendsApplication::begin(events::EventBusPtr eventBus_, display::LcdPtr lcd_)
{
    eventBus = eventBus_;
    lcd = lcd_;

    // Custom subscriptions
    subscriptionInfos.push_back(eventBus->subscribe(buttons::BTN_SWIPED_EVENT, std::make_shared<ShouldSendInfoEventHandler>(*this)));
    subscriptionInfos.push_back(eventBus->subscribe(ir::IR_MESSAGE_RECEIVED_EVENT, std::make_shared<FriendMetEventHandler>(*this)));

    eventBus->publish(std::make_shared<ble::RequestBleServiceCreateEvent>(config::friendsServiceUuid));
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicCreateEvent>(config::friendsServiceUuid, config::friendsCharacteristcUuid, ble::R, serializeLastFriends()));
    eventBus->publish(std::make_shared<ble::RequestBleServiceStartEvent>(config::friendsServiceUuid));
    drawGoodLuck();

    std::string serializedFriends = info::get_persistent("friends");

    deserializeFriends(serializedFriends);
    eventBus->publish(std::make_shared<ble::RequestBleCharacteristicUpdateEvent>
        (config::friendsCharacteristcUuid, serializeLastFriends()));
}

void FriendsApplication::finish()
{
    for (auto subscriptionInfo : subscriptionInfos)
    {
        eventBus->unsubscribe(subscriptionInfo);
    }
}

void FriendsApplication::drawPreview(display::LcdPtr lcd)
{
    lcd->erase();
    lcd->draw2HebrewSentences("תורכיה", "");
    lcd->drawXBM(
		(lcd->getDisplayWidth() - friends_im_width) / 2,
		(lcd->getDisplayHeight()) / 2,
		friends_im_width,
		friends_im_height,
		friends_im_bits);
    lcd->render();
}

ApplicationPtr FriendsApplication::create()
{
    return std::make_shared<FriendsApplication>();
}
