#pragma once

#include <Application.hpp>
#include <Thread.hpp>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <Logging.hpp>

struct Friend
{
	Friend(std::string firstName, std::string lastName, std::string id)
		: firstName(firstName), lastName(lastName), id(id)
	{

	}
	Friend(std::string data)
	{
		auto result1 = std::find(std::begin(data), std::end(data), '-');
		firstName = std::string(std::begin(data), result1);

		result1++;
		auto result2 = std::find(result1, std::end(data), '-');
		lastName = std::string(result1, result2);

		result2++;
		auto result3 = std::find(result2, std::end(data), '-');
		id = std::string(result2, result3);
	}
	bool operator==(const Friend& afriend)
	{
		return firstName == afriend.firstName && lastName == afriend.lastName && id == id;
	}

	bool operator!=(const Friend& afriend)
	{
		return !(*this == afriend);
	}

	std::string serialize()
	{
		return firstName + "-" + lastName + "-" + id;	
	}

	std::string firstName;
	std::string lastName;
	std::string id;
};

class FriendsApplication : public Application
{
public:
    void begin(events::EventBusPtr, display::LcdPtr);
    void finish();

    static void drawPreview(display::LcdPtr);
    static ApplicationPtr create();

    friend class FriendMetEventHandler;
    friend class ShouldSendInfoEventHandler;

private:
	void friendMet(Friend afriend);
	void drawGoodLuck();
	void drawFriendAnimation();
	std::string serializeLastFriends();
	
	std::string serializeFriends();
	void deserializeFriends(std::string);
	
	void sendLastFriends();
	void sendInfo();

	events::EventBusPtr eventBus = nullptr;
	display::LcdPtr lcd = nullptr;
	std::vector<events::SubscriptionInfo> subscriptionInfos = {};
	std::vector<Friend> friends = {};

	static const std::vector<Friend>::size_type N_LAST_FRIENDS = 20;
};
