// Core
#include <Arduino.h>
#include <EventBus.hpp>
#include <Lcd.hpp>

// Utillities
#include <Thread.hpp>
#include <Logging.hpp>
#include <Info.hpp>
// #include <Filesystem.hpp> //@todesign

// Modules
#include <ButtonsModule.hpp>
#include <IrModule.hpp>
#include <BleModule.hpp>
#include <BatteryModule.hpp>

// Applications
#include <BootApplication.hpp>
#include <MenuApplication.hpp>

void mainLogic()
{
    // Utillities
    logging::initialize(Info);
    info::initialize();
    // filesystem::initialize(); //@todesign

    logging::debug("Setting up system\n");
    // Core
    events::EventBusPtr eventBus = std::make_shared<events::EventBus>();
    logging::debug("EventBus created\n");
    display::LcdPtr lcd = std::make_shared<display::Lcd>();
    delay(1000);
    lcd->begin();
    logging::debug("Display initialized\n");

    // Modules
    ModulePtr btnModule = std::make_shared<buttons::ButtonsModule>();
    ModulePtr irModule = std::make_shared<ir::IrModule>();
    ModulePtr bleModule = std::make_shared<ble::BleModule>();
    ModulePtr batteryModule = std::make_shared<battery::BatteryModule>();
    logging::debug("Modules created\n");

    btnModule->begin(eventBus);
    irModule->begin(eventBus);
    bleModule->begin(eventBus);
    batteryModule->begin(eventBus);
    logging::debug("Modules initialized\n");

    logging::debug("Starting bootApp\n");
    // Boot app
    ApplicationPtr bootApp = std::make_shared<BootApplication>();
    bootApp->begin(eventBus, lcd); // boot app never finishes.

    logging::debug("Starting MenuApp\n");
    // Menu app
    ApplicationPtr menuApp = std::make_shared<MenuApplication>();
    menuApp->begin(eventBus, lcd); // menu app never finishes.


    logging::debug("Starting run loop\n");
    unsigned long last_battery_update_time = 0;
    while(1)
    {
        // Update logic
        unsigned long current_time = millis();
        btnModule->update();
        irModule->update();
        bleModule->update();
        if ((current_time - last_battery_update_time) > 1000) {
            batteryModule->update();
            last_battery_update_time = current_time;
        }
    }
}

void threadFunction(void *pvParameters)
{
    mainLogic();
}

void setup()
{
    // @todo:
    // thread or not?
    // we should use threads in general, how many threads can we use?

    // arduino-esp32 runs the "loopTask" with a stack sized 8k (framework-arduinoespressif32/cores/esp32/main.cpp).
    // but we want more, so we create our own task and give it a bigger stack ()
    // multithreading::Thread thread(threadFunction, nullptr, 8192);
    // while(1);

    mainLogic(); // @todo
}

void loop()
{
}
