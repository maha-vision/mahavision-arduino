# Mahavision
Mahavision is a cheap smart glasses kit.

The demos folder contains examples for various components.

The Mahavision folder contains the firmware code. 

See the Wiki (https://gitlab.com/maha-vision/mahavision-arduino/wikis/home) for build instructions and more.

The Webapp can be found at https://gitlab.com/maha-vision/mahavision-webapp.